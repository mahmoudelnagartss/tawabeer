import 'package:flutter/material.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

enum TextColor {
  blackColor,
  greyColor,
  appMainColor,
  whiteColor,
}

class ColoredTitleTextWidget extends StatelessWidget {
  final String text;
  final double? fontSize;
  final int? numberOfLines;
  final FontWeight? fontWeight;
  final TextColor? color;
  final VoidCallback? onClick;

  const ColoredTitleTextWidget({
    Key? key,
    required this.text,
    this.fontSize,
    this.fontWeight = FontWeight.normal,
    this.numberOfLines,
    this.color,
    this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isContainingNumbers = text.contains(
      RegExp(r'[0-9]'),
    );

    Color _textColor = Colors.grey.shade600;
    if (color != null) {
      if (color == TextColor.blackColor) {
        _textColor = Colors.black;
      } else if (color == TextColor.appMainColor) {
        _textColor = AppColors.primaryColor.shade600;
      } else if (color == TextColor.whiteColor) {
        _textColor = Colors.white;
      }
    }

    return Card(
      elevation: 0.0,
      margin: const EdgeInsets.all(0.0),
      color: Colors.transparent,
      child: Text(
        text,
        maxLines: numberOfLines ?? 1,
        style: TextStyle(
          fontFamilyFallback: const ['Roboto'],
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: _textColor,
          fontFamily: isContainingNumbers ? null : AppLocalizations.of(context)!.fontName,
          letterSpacing: -1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
