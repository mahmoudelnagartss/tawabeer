import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';

class DropdownMenuLabeledWidget extends StatefulWidget {
  final String title;
  final List<String> list;
  final double? width;

  const DropdownMenuLabeledWidget({
    Key? key,
    required this.title,
    required this.list,
    this.width,
  }) : super(key: key);

  @override
  State<DropdownMenuLabeledWidget> createState() =>
      _DropdownMenuLabeledWidgetState();
}

class _DropdownMenuLabeledWidgetState extends State<DropdownMenuLabeledWidget> {
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ColoredTitleTextWidget(
          text: widget.title,
          fontWeight: FontWeight.bold,
        ),
        const SizedBox(
          height: 8,
        ),
        Container(
          height: 50,
          width: widget.width ?? 300,
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          decoration: BoxDecoration(
            color: Colors.grey.shade100,
            borderRadius: const BorderRadius.all(
              Radius.circular(30.0),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: (widget.width ?? 300) - 120,
                child: DropdownButton<String>(
                  icon: const SizedBox.shrink(),
                  iconSize: 20,
                  elevation: 16,
                  underline: Container(),
                  value:
                      (selectedValue == null) ? widget.list[0] : selectedValue,
                  items: widget.list.map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child:  ColoredTitleTextWidget(
                        text: value,
                      ),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      selectedValue = value ?? '';
                    });
                  },
                ),
              ),
              Icon(
                Icons.arrow_drop_down_sharp,
                color: Colors.grey.shade600,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
