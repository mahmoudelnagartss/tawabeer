import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class CustomSliverAppBar extends StatelessWidget {
  final double appBarHeight;
  final String title;

  const CustomSliverAppBar({
    Key? key,
    required this.appBarHeight,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      pinned: true,
      expandedHeight: appBarHeight,
      floating: true,
      centerTitle: true,
      backgroundColor: AppColors.primaryColor.shade600,
      flexibleSpace: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          int percent = ((constraints.maxHeight - kToolbarHeight) *
              100 ~/ (appBarHeight - kToolbarHeight));

          if (percent < 0) {
            percent = 0;
          } else if (percent > 100) {
            percent = 100;
          }

          double leftSide = 65 - (percent.toDouble() / 2);
          debugPrint('percent:$percent leftSide:$leftSide');

          double fontSize = 8 + (percent / 4);
          if (fontSize < 15) {
            fontSize = 15;
          } else if (fontSize > 25) {
            fontSize = 25;
          }

          return Stack(
            children: <Widget>[
              Image.asset(
                "assets/bgProduct.png",
                fit: BoxFit.cover,
                height: double.infinity,
                width: double.infinity,
              ),
              Positioned(
                bottom: 12,
                left: leftSide,
                right: leftSide,
                child: ColoredTitleTextWidget(
                  text: title,
                  fontWeight: FontWeight.bold,
                  fontSize: fontSize,
                  color: TextColor.whiteColor,
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
