import 'package:flutter/material.dart';

class TopCorneredContainerWidget extends StatelessWidget {
  final Widget childWidget;
  final double? height;
  final double? width;

  const TopCorneredContainerWidget({Key? key, required this.childWidget, this.height, this.width }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      height: (height != null) ? height : MediaQuery.of(context).size.height,
      width: (width != null) ? width : MediaQuery.of(context).size.width,
      padding: const EdgeInsets.fromLTRB(8, 16, 8, 0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      child: childWidget,
    );
  }
}
