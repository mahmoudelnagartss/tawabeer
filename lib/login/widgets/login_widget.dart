import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/Models/SocialUser.dart';
import 'package:tawabeer/login/Presenter/AuthPresenter.dart';
import 'package:tawabeer/login/widgets/forgot_password_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class LoginWidget extends StatefulWidget {
  final Function switchViews;

  const LoginWidget({
    Key? key,
    required this.switchViews,
  }) : super(key: key);

  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  bool isSecurePassword = true;

  @override
  Widget build(BuildContext context) {
    final _appProvider = Provider.of<AppProvider>(context, listen: false);
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.login,
        height: 60,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 16,
                ),
                Container(
                  height: 70,
                  width: _screenWidth * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  child: Center(
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.adsArea,
                      fontSize: 16.0,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Center(
                  child: ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.signInOptions,
                    fontSize: 16.0,
                  ),
                ),
                const SizedBox(
                  height: 16.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        SocialUser _socialLogin = await AuthPresenter().signUserInWithFacebook();
                        debugPrint('isNewUser- ${_socialLogin.isNewUser}');
                        debugPrint('firstName- ${_socialLogin.firstName}');
                        debugPrint('lastName- ${_socialLogin.lastName}');
                        debugPrint('email- ${_socialLogin.email}');
                        debugPrint('photoURL- ${_socialLogin.photoURL}');
                        debugPrint('userId- ${_socialLogin.userId}');
                        debugPrint('phoneNumber- ${_socialLogin.phoneNumber}');
                        debugPrint('displayName- ${_socialLogin.displayName}');

                        showSimpleNotification(
                          ListTile(
                            leading: CircleAvatar(
                              radius: 25.0,
                              backgroundColor: AppColors.primaryColor.shade600,
                              backgroundImage: NetworkImage(_socialLogin.photoURL),
                            ),
                            title: Text(_socialLogin.displayName),
                            subtitle: Text(_socialLogin.email),
                          ),
                          duration: const Duration(seconds: 5),
                          background: Colors.grey.shade600,
                          elevation: 2.0,
                        );
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          color: Colors.blue.shade800,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: const Center(
                          child: FaIcon(
                            FontAwesomeIcons.facebookF,
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    GestureDetector(
                      onTap: () async {
                        SocialUser _socialLogin = await AuthPresenter().signUserInWithGoogle();
                        debugPrint('isNewUser- ${_socialLogin.isNewUser}');
                        debugPrint('firstName- ${_socialLogin.firstName}');
                        debugPrint('lastName- ${_socialLogin.lastName}');
                        debugPrint('email- ${_socialLogin.email}');
                        debugPrint('photoURL- ${_socialLogin.photoURL}');
                        debugPrint('userId- ${_socialLogin.userId}');
                        debugPrint('phoneNumber- ${_socialLogin.phoneNumber}');
                        debugPrint('displayName- ${_socialLogin.displayName}');

                        showSimpleNotification(
                          ListTile(
                            leading: CircleAvatar(
                              radius: 25.0,
                              backgroundColor: AppColors.primaryColor.shade600,
                              backgroundImage: NetworkImage(_socialLogin.photoURL),
                            ),
                            title: Text(_socialLogin.displayName),
                            subtitle: Text(_socialLogin.email),
                          ),
                          duration: const Duration(seconds: 5),
                          background: Colors.grey.shade600,
                          elevation: 2.0,
                        );
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: BoxDecoration(
                          color: Colors.red.shade800,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: const Center(
                          child: FaIcon(
                            FontAwesomeIcons.google,
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 16,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    (defaultTargetPlatform == TargetPlatform.iOS) ? GestureDetector(
                      onTap: () async {
                        SocialUser _socialLogin = await AuthPresenter().signUserInWithApple();

                        debugPrint('_credential $_socialLogin');

                        debugPrint('isNewUser- ${_socialLogin.isNewUser}');
                        debugPrint('firstName- ${_socialLogin.firstName}');
                        debugPrint('lastName- ${_socialLogin.lastName}');
                        debugPrint('email- ${_socialLogin.email}');
                        debugPrint('photoURL- ${_socialLogin.photoURL}');
                        debugPrint('userId- ${_socialLogin.userId}');
                        debugPrint('phoneNumber- ${_socialLogin.phoneNumber}');
                        debugPrint('displayName- ${_socialLogin.displayName}');

                        showSimpleNotification(
                          ListTile(
                            leading: CircleAvatar(
                              radius: 25.0,
                              backgroundColor: AppColors.primaryColor.shade600,
                              backgroundImage: const AssetImage('assets/logo.png'),
                            ),
                            title: Text('${_socialLogin.firstName} ${_socialLogin.lastName}'),
                            subtitle: Text(_socialLogin.email),
                          ),
                          duration: const Duration(seconds: 5),
                          background: Colors.grey.shade600,
                          elevation: 2.0,
                        );
                      },
                      child: Container(
                        height: 50,
                        width: 50,
                        decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(
                            Radius.circular(25.0),
                          ),
                        ),
                        child: const Center(
                          child: FaIcon(
                            FontAwesomeIcons.apple,
                            color: Colors.white,
                            size: 20,
                          ),
                        ),
                      ),
                    ) : const SizedBox.shrink(),

                  ],
                ),


                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 1,
                      width: 50,
                      color: Colors.grey,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.or,
                      fontWeight: FontWeight.w700,
                      fontSize: 16.0,
                    ),
                    Container(
                      height: 1,
                      width: 50,
                      color: Colors.grey,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.phone_iphone,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 60,
                          child: TextField(
                            onChanged: (text) async {},
                            keyboardType:
                            TextInputType.phone,
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText:
                                  AppLocalizations.of(context)!.phoneNumber,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecurePassword,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText: AppLocalizations.of(context)!.password,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecurePassword = !isSecurePassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                SizedBox(
                  width: _screenWidth * 0.8,
                  child: Row(
                    children: [
                      const Spacer(),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            CupertinoPageRoute(
                              maintainState: true,
                              builder: (BuildContext context) {
                                return const ForgotPasswordWidget();
                              },
                            ),
                          );
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.forgotPassword,
                          color: TextColor.blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: double.infinity),
                    child: TextButton(
                      onPressed: () {
                        _appProvider.setAppHomeWidget(AppHomeWidgets.tabBar);
                        // Navigator.of(context).pushReplacement(
                        //   MaterialPageRoute(
                        //     maintainState: true,
                        //     builder: (BuildContext context) {
                        //       return const MainTabBarWidget();
                        //     },
                        //     settings: const RouteSettings(
                        //       name: 'MainTabBarWidget',
                        //     ),
                        //   ),
                        // );

                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.login,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: TextColor.whiteColor,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: _screenWidth * 0.8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.noAccount,
                        color: TextColor.blackColor,
                      ),
                      TextButton(
                        onPressed: () {
                          widget.switchViews();
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.register,
                          color: TextColor.appMainColor,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
