import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/widgets/forgot_password_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChangePasswordWidget extends StatefulWidget {
  const ChangePasswordWidget({Key? key}) : super(key: key);

  @override
  _ChangePasswordWidgetState createState() => _ChangePasswordWidgetState();
}

class _ChangePasswordWidgetState extends State<ChangePasswordWidget> {
  bool isSecureCurrentPassword = true;
  bool isSecurePassword = true;
  bool isSecureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.passwordChange,
        height: 60,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.9) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecureCurrentPassword,
                            style: TextStyle(
                              fontFamily: AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!
                                    .fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),

                              hintText:
                                  AppLocalizations.of(context)!.currentPassword,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecureCurrentPassword =
                                  !isSecureCurrentPassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  width: _screenWidth * 0.9,
                  child: Row(
                    children: [
                      const Spacer(),
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              maintainState: true,
                              builder: (BuildContext context) {
                                return const ForgotPasswordWidget();
                              },
                            ),
                          );
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.forgotPassword,
                          color: TextColor.blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.enterNewPassword,
                      fontWeight: FontWeight.w400,
                      fontSize: 16.0,
                      color: TextColor.appMainColor,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.9) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecurePassword,
                            style: TextStyle(
                              fontFamily: AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!
                                    .fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),

                              hintText:
                                  AppLocalizations.of(context)!.newPassword,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecurePassword = !isSecurePassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.9,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.9) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecureConfirmPassword,
                            style: TextStyle(
                              fontFamily: AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!
                                    .fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),

                              hintText:
                                  AppLocalizations.of(context)!.passwordConfirm,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecureConfirmPassword =
                                  !isSecureConfirmPassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.6,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: double.infinity),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.savePassword,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: TextColor.whiteColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
