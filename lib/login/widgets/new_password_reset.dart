import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NewPasswordResetWidget extends StatefulWidget {
  const NewPasswordResetWidget({Key? key}) : super(key: key);

  @override
  _NewPasswordResetWidgetState createState() => _NewPasswordResetWidgetState();
}

class _NewPasswordResetWidgetState extends State<NewPasswordResetWidget> {
  bool isSecurePassword = true;
  bool isSecureConfirmPassword = true;

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.newPassword,
        height: 60,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.resetNewPassword,
                      fontWeight: FontWeight.w700,
                      fontSize: 18.0,
                      color: TextColor.blackColor,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.enterNewPassword,
                      fontSize: 14.0,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecurePassword,
                            style: TextStyle(
                              fontFamily: AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!
                                    .fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),

                              hintText:
                                  AppLocalizations.of(context)!.newPassword,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecurePassword = !isSecurePassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecureConfirmPassword,
                            style: TextStyle(
                              fontFamily: AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!
                                    .fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),

                              hintText:
                                  AppLocalizations.of(context)!.passwordConfirm,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecureConfirmPassword =
                                  !isSecureConfirmPassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.6,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: double.infinity),
                    child: TextButton(
                      onPressed: () {

                        Navigator.of(context).popUntil((route) {
                          return route.isFirst;
                        });
                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.confirm,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: TextColor.whiteColor,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
