import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/widgets/login_widget.dart';
import 'package:tawabeer/login/widgets/register_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class AuthenticationWrapperWidget extends StatefulWidget {
  const AuthenticationWrapperWidget({Key? key}) : super(key: key);

  @override
  _AuthenticationWrapperWidgetState createState() =>
      _AuthenticationWrapperWidgetState();
}

class _AuthenticationWrapperWidgetState
    extends State<AuthenticationWrapperWidget> {
  bool showSignIn = true;

  void switchViews() {
    setState(() {
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return LoginWidget(
        switchViews: switchViews,
      );
    } else {
      return RegisterWidget(
        switchViews: switchViews,
      );
    }

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: const AppBarWidget(
        barTitle: 'Login',
        height: 60,
      ),
      body: TopCorneredContainerWidget(
        childWidget: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.blue.shade800,
                borderRadius: const BorderRadius.all(
                  Radius.circular(30.0),
                ),
              ),
              child: Center(
                child: Row(
                  children: const [
                    Icon(
                      Icons.facebook,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Login with Facebook',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 5,
            ),
          ],
        ),
      ),
    );
  }
}
