import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/Models/SocialUser.dart';
import 'package:tawabeer/login/Presenter/AuthPresenter.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class RegisterWidget extends StatefulWidget {
  final Function switchViews;

  const RegisterWidget({
    Key? key,
    required this.switchViews,
  }) : super(key: key);

  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {
  bool isSecurePassword = true;
  bool isSecureConfirmPassword = true;
  final TextEditingController _controllerForName = TextEditingController();
  final TextEditingController _controllerForEmail = TextEditingController();
  final TextEditingController _controllerForPhone = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _appProvider = Provider.of<AppProvider>(context, listen: false);
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.register,
        height: 60,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.orFillData,
                  fontWeight: FontWeight.w700,
                  fontSize: 16.0,
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.person_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 60,
                          child: TextField(
                            onChanged: (text) async {},
                            controller: _controllerForName,
                            keyboardType: TextInputType.name,
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText: AppLocalizations.of(context)!.name,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.email_outlined,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 60,
                          child: TextField(
                            onChanged: (text) async {},
                            controller: _controllerForEmail,
                            keyboardType: TextInputType.emailAddress,
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText: AppLocalizations.of(context)!.email,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.phone_iphone,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 60,
                          child: TextField(
                            onChanged: (text) async {},
                            controller: _controllerForPhone,
                            keyboardType: TextInputType.phone,
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            // textAlignVertical: TextAlignVertical.center,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText:
                                  AppLocalizations.of(context)!.phoneNumber,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecurePassword,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText: AppLocalizations.of(context)!.password,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecurePassword = !isSecurePassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.8,
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: Center(
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 20,
                        ),
                        Icon(
                          Icons.lock_outline,
                          color: Colors.grey.shade600,
                          size: 20,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        SizedBox(
                          width: (_screenWidth * 0.8) - 120,
                          child: TextField(
                            onChanged: (text) async {},
                            textInputAction: TextInputAction.next,
                            cursorColor: AppColors.primaryColor.shade600,
                            expands: false,
                            maxLines: 1,
                            minLines: 1,
                            obscureText: isSecureConfirmPassword,
                            style: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                            ),
                            decoration: InputDecoration(
                              hintStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              labelStyle: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              hintText:
                                  AppLocalizations.of(context)!.passwordConfirm,
                              focusColor: Colors.brown,
                              filled: true,
                              fillColor: Colors.grey.shade100,
                              contentPadding:
                                  const EdgeInsets.fromLTRB(0, 5, 0, 5),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              isSecureConfirmPassword =
                                  !isSecureConfirmPassword;
                            });
                          },
                          icon: Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 50,
                  width: _screenWidth * 0.6,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  child: ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: double.infinity),
                    child: TextButton(
                      onPressed: () {
                        _appProvider.setAppHomeWidget(AppHomeWidgets.tabBar);
                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.register,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: TextColor.whiteColor,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: _screenWidth * 0.8,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.haveAccount,
                        color: TextColor.blackColor,
                      ),
                      TextButton(
                        onPressed: () {
                          widget.switchViews();
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.login,
                          color: TextColor.appMainColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
