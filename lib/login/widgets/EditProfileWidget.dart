
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/widgets/ChangePasswordWidget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/services/app_permissions_services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class EditProfileWidget extends StatefulWidget {
  const EditProfileWidget({Key? key}) : super(key: key);

  @override
  _EditProfileWidgetState createState() => _EditProfileWidgetState();
}

class _EditProfileWidgetState extends State<EditProfileWidget> {
  final ImagePicker _picker = ImagePicker();
  XFile? _imageFile;
  String filePath = '';

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.editProfile,
        height: 100,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: SizedBox.fromSize(
                          size: const Size.fromRadius(60),
                          child: (_imageFile == null)
                              ? Image.asset(
                                  'assets/photo_food_0.jpeg',
                                  fit: BoxFit.cover,
                                )
                              : Image.file(
                                  File(_imageFile?.path ?? ''),
                                  fit: BoxFit.cover,
                                ),
                        ),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ColoredTitleTextWidget(
                            text: AppLocalizations.of(context)!.changePhoto,
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                            color: TextColor.blackColor,
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                          Row(
                            children: [
                              Container(
                                height: 40,
                                width: _screenWidth * 0.15,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryColor.shade50,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0),
                                  ),
                                ),
                                child: ConstrainedBox(
                                  constraints: const BoxConstraints(
                                    minWidth: double.infinity,
                                  ),
                                  child: IconButton(
                                    onPressed: () async {
                                      bool isPermissionGranted =
                                          await AppPermissionsServices()
                                              .requestCameraPermission();
                                      if (isPermissionGranted) {
                                        final XFile? image =
                                            await _picker.pickImage(
                                          source: ImageSource.camera,
                                          // source: ImageSource.gallery,
                                        );
                                        setState(() {
                                          _imageFile = image;
                                        });
                                      }
                                    },
                                    icon: Icon(
                                      Icons.camera_enhance,
                                      color: AppColors.primaryColor.shade800,
                                    ),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                              Container(
                                height: 40,
                                width: _screenWidth * 0.15,
                                decoration: BoxDecoration(
                                  color: AppColors.primaryColor.shade50,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(8.0),
                                  ),
                                ),
                                child: ConstrainedBox(
                                  constraints: const BoxConstraints(
                                    minWidth: double.infinity,
                                  ),
                                  child: IconButton(
                                    onPressed: () async {
                                      bool isPermissionGranted =
                                          await AppPermissionsServices()
                                              .requestPhotosPermission();
                                      if (isPermissionGranted) {
                                        final XFile? image =
                                            await _picker.pickImage(
                                          source: ImageSource.gallery,
                                        );
                                        setState(() {
                                          _imageFile = image;
                                        });
                                      }
                                    },
                                    icon: Icon(
                                      Icons.photo_library_outlined,
                                      color: AppColors.primaryColor.shade800,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(30.0),
                      ),
                    ),
                    child: Center(
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.person_outline,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          SizedBox(
                            width: _screenWidth - 100,
                            child: TextField(
                              onChanged: (text) async {},
                              keyboardType: TextInputType.name,
                              textInputAction: TextInputAction.next,
                              cursorColor: AppColors.primaryColor.shade600,
                              // textAlignVertical: TextAlignVertical.center,
                              expands: false,
                              maxLines: 1,
                              minLines: 1,
                              style: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),
                              decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontFamily: AppLocalizations.of(context)!
                                      .fontName,
                                ),
                                labelStyle: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontFamily: AppLocalizations.of(context)!.fontName,
                                ),
                                labelText: AppLocalizations.of(context)!.name,
                                focusColor: Colors.brown,
                                filled: true,
                                fillColor: Colors.grey.shade100,
                                contentPadding:
                                    const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(30.0),
                      ),
                    ),
                    child: Center(
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.email_outlined,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          SizedBox(
                            width: _screenWidth - 100,
                            child: TextField(
                              onChanged: (text) async {},
                              keyboardType: TextInputType.emailAddress,
                              textInputAction: TextInputAction.next,
                              cursorColor: AppColors.primaryColor.shade600,
                              // textAlignVertical: TextAlignVertical.center,
                              expands: false,
                              maxLines: 1,
                              minLines: 1,
                              style: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),
                              decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontFamily: AppLocalizations.of(context)!
                                      .fontName,
                                ),
                                labelStyle: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontFamily: AppLocalizations.of(context)!.fontName,
                                ),
                                labelText: AppLocalizations.of(context)!.email,
                                focusColor: Colors.brown,
                                filled: true,
                                fillColor: Colors.grey.shade100,
                                contentPadding:
                                    const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(30.0),
                      ),
                    ),
                    child: Center(
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.phone_iphone,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          SizedBox(
                            width: _screenWidth - 100,
                            child: TextField(
                              onChanged: (text) async {},
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.next,
                              cursorColor: AppColors.primaryColor.shade600,
                              // textAlignVertical: TextAlignVertical.center,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                  RegExp(
                                    "[0-9]",
                                  ),
                                ),
                              ],
                              expands: false,
                              maxLines: 1,
                              minLines: 1,
                              style: TextStyle(
                                fontFamily: AppLocalizations.of(context)!.fontName,
                              ),
                              decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontFamily: AppLocalizations.of(context)!
                                      .fontName,
                                ),
                                labelStyle: TextStyle(
                                  color: Colors.grey.shade600,
                                  fontFamily: AppLocalizations.of(context)!.fontName,
                                ),
                                labelText:
                                    AppLocalizations.of(context)!.phoneNumber,
                                focusColor: Colors.brown,
                                filled: true,
                                fillColor: Colors.grey.shade100,
                                contentPadding:
                                    const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.passwordChange,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                        color: TextColor.blackColor,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor.shade50,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(25.0),
                      ),
                    ),
                    child: ConstrainedBox(
                      constraints:
                          const BoxConstraints(minWidth: double.infinity),
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            CupertinoPageRoute(
                              maintainState: true,
                              builder: (BuildContext context) {
                                return const ChangePasswordWidget();
                              },
                            ),
                          );
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const SizedBox(
                              width: 20,
                            ),
                            Icon(
                              Icons.lock_outline_rounded,
                              color: AppColors.primaryColor.shade600,
                              size: 20,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            ColoredTitleTextWidget(
                              text: AppLocalizations.of(context)!.passwordChange,
                              fontSize: 16.0,
                              color: TextColor.appMainColor,
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor.shade600,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(25.0),
                      ),
                    ),
                    child: ConstrainedBox(
                      constraints:
                          const BoxConstraints(minWidth: double.infinity),
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.saveChanges,
                          fontWeight: FontWeight.w700,
                          fontSize: 16.0,
                          color: TextColor.whiteColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
