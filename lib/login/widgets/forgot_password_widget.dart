import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/widgets/verify_code_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class ForgotPasswordWidget extends StatefulWidget {
  const ForgotPasswordWidget({Key? key}) : super(key: key);

  @override
  _ForgotPasswordWidgetState createState() => _ForgotPasswordWidgetState();
}

class _ForgotPasswordWidgetState extends State<ForgotPasswordWidget> {
  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.forgotPasswordTitle,
        height: 100,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Image.asset(
                        'assets/forgetPassword.png',
                        height: 120,
                        fit: BoxFit.fitHeight,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.forgotPasswordDesc,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                        color: TextColor.blackColor,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.enterPhoneNumber,
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  Container(
                    height: 50,
                    width: _screenWidth * 0.8,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade100,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(25.0),
                      ),
                    ),
                    child: Center(
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          Icon(
                            Icons.phone_iphone,
                            color: Colors.grey.shade600,
                            size: 20,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          SizedBox(
                            width: (_screenWidth * 0.8) - 60,
                            child: TextField(
                              onChanged: (text) async {},
                              keyboardType:
                                  const TextInputType.numberWithOptions(),
                              textInputAction: TextInputAction.next,
                              cursorColor: AppColors.primaryColor.shade600,
                              // textAlignVertical: TextAlignVertical.center,
                              expands: false,
                              maxLines: 1,
                              minLines: 1,
                              style: TextStyle(
                                fontFamily:
                                    AppLocalizations.of(context)!.fontName,
                              ),
                              decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontFamily:
                                      AppLocalizations.of(context)!.fontName,
                                ),
                                labelStyle: TextStyle(
                                  fontFamily:
                                      AppLocalizations.of(context)!.fontName,
                                ),
                                hintText:
                                    AppLocalizations.of(context)!.phoneNumber,
                                focusColor: Colors.brown,
                                filled: true,
                                fillColor: Colors.grey.shade100,
                                contentPadding:
                                    const EdgeInsets.fromLTRB(0, 5, 0, 5),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Container(
                    height: 50,
                    width: _screenWidth * 0.8,
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor.shade600,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(25.0),
                      ),
                    ),
                    child: ConstrainedBox(
                      constraints:
                          const BoxConstraints(minWidth: double.infinity),
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).push(
                            CupertinoPageRoute(
                              maintainState: true,
                              builder: (BuildContext context) {
                                return const VerifyCodeWidget();
                              },
                            ),
                          );
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.send,
                          fontWeight: FontWeight.w700,
                          fontSize: 16.0,
                          color: TextColor.whiteColor,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
