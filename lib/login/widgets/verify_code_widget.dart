import 'dart:async';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/login/widgets/new_password_reset.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class VerifyCodeWidget extends StatefulWidget {
  const VerifyCodeWidget({Key? key}) : super(key: key);

  @override
  _VerifyCodeWidgetState createState() => _VerifyCodeWidgetState();
}

class _VerifyCodeWidgetState extends State<VerifyCodeWidget> {
  TextEditingController textEditingController = TextEditingController();
  StreamController<ErrorAnimationType>? errorController;

  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.codeVerify,
        height: 100,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: TopCorneredContainerWidget(
          childWidget: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Image.asset(
                        'assets/sendCode.png',
                        height: 120,
                        fit: BoxFit.fitHeight,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.codeVerifyDesc,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                        color: TextColor.blackColor,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.enterVerifyCode,
                          fontSize: 16.0,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const ColoredTitleTextWidget(
                        text: '+966 51235468  ',
                        color: TextColor.appMainColor,
                      ),
                      Container(
                        width: 150,
                        height: 40,
                        decoration: BoxDecoration(
                          color: Colors.grey.shade200,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20.0),
                          ),
                        ),
                        child: Center(
                          child: ConstrainedBox(
                            constraints:
                                const BoxConstraints(minWidth: double.infinity),
                            child: TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: ColoredTitleTextWidget(
                                text:
                                    AppLocalizations.of(context)!.changeNumber,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: _screenWidth * 0.8,
                    child: PinCodeTextField(
                      appContext: context,
                      length: 4,
                      obscureText: false,
                      errorTextSpace: 5,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(
                          RegExp(
                            "[0-9]",
                          ),
                        ),
                      ],
                      animationType: AnimationType.scale,
                      animationDuration: const Duration(milliseconds: 300),
                      backgroundColor: Colors.transparent,
                      // background for the whole widget
                      cursorColor: Colors.white,
                      enableActiveFill: true,
                      textStyle: const TextStyle(
                        color: Colors.white,
                      ),
                      errorAnimationController: errorController,
                      controller: textEditingController,
                      onCompleted: (value) {
                        debugPrint(value);
                        setState(() {
                          isActive = true;
                        });
                      },
                      onChanged: (value) {
                        debugPrint(value);
                        setState(() {
                          isActive = (value.length == 4);
                        });
                      },
                      beforeTextPaste: (text) {
                        debugPrint("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return false;
                      },
                      pinTheme: PinTheme(
                        fieldHeight: 50,
                        fieldWidth: 50,
                        // borderRadius: BorderRadius.circular(5),
                        shape: PinCodeFieldShape.circle,
                        // background color in case of the fields is not enabled
                        disabledColor: Colors.red.shade600,
                        //background colors
                        inactiveFillColor: Colors.grey.shade200,
                        // when not selected and not filled
                        activeFillColor: Colors.grey.shade800,
                        // when not selected but filled
                        selectedFillColor: AppColors.primaryColor.shade600,
                        // when selected
                        // border colors
                        inactiveColor: Colors.white,
                        // when not selected and not filled
                        activeColor: Colors.white,
                        // when not selected but filled
                        selectedColor: Colors.white,
                        // when selected
                        //error color
                        errorBorderColor: Colors.red.shade600,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Container(
                    height: 50,
                    width: _screenWidth * 0.8,
                    decoration: BoxDecoration(
                      color: isActive
                          ? AppColors.primaryColor.shade600
                          : Colors.grey.shade600,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(25.0),
                      ),
                    ),
                    child: ConstrainedBox(
                      constraints:
                          const BoxConstraints(minWidth: double.infinity),
                      child: TextButton(
                        onPressed: () {
                          if (isActive) {
                            Navigator.of(context).push(
                              CupertinoPageRoute(
                                maintainState: true,
                                builder: (BuildContext context) {
                                  return const NewPasswordResetWidget();
                                },
                              ),
                            );
                          }
                        },
                        child: ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.send,
                          fontWeight: FontWeight.w700,
                          fontSize: 16.0,
                          color: TextColor.whiteColor,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.codeNotSent,
                    fontSize: 16.0,
                    color: TextColor.blackColor,
                  ),
                  ConstrainedBox(
                    constraints:
                        const BoxConstraints(minWidth: double.infinity),
                    child: TextButton(
                      onPressed: () {},
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.resendCode,
                        fontWeight: FontWeight.w700,
                        fontSize: 16.0,
                        color: TextColor.appMainColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
