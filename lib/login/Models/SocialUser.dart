import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class SocialUser {
  bool isNewUser = true;
  String firstName = '';
  String lastName = '';
  String displayName = '';
  String email = '';
  String userId = '';
  String photoURL = '';
  String phoneNumber = '';

  SocialUser(UserCredential userCredential) {

    debugPrint('isNewUser- $userCredential');
    User? _userData = userCredential.user;
    if (_userData != null) {
      userId = _userData.uid;
      displayName = _userData.displayName ?? '';
      email = _userData.email ?? '';
      phoneNumber = _userData.phoneNumber ?? '';
      photoURL = _userData.photoURL ?? '';
    }

    AdditionalUserInfo? _additionalUserInfo = userCredential.additionalUserInfo;
    if (_additionalUserInfo != null) {
      isNewUser = _additionalUserInfo.isNewUser;

      Map? _profile = userCredential.additionalUserInfo!.profile;
      if (_profile != null) {
        firstName = _profile['first_name'] ?? '';
        lastName = _profile['last_name'] ?? '';

        if(firstName.isEmpty) {
          firstName = _profile['given_name'] ?? '';
        }

        if(lastName.isEmpty) {
          lastName = _profile['family_name'] ?? '';
        }
        // email = _profile['email'];
        // userId = _profile['id'];
        //
        // Map? _picture = _profile['picture'];
        // if (_picture != null) {
        //   Map? _pictureData = _picture['data'];
        //   if (_pictureData != null) {
        //     photoURL = _pictureData['url'];
        //   }
        // }
      }
    }
  }
}

