
import 'package:flutter/foundation.dart';

enum AuthenticationWidgets {
  login,
  register,
}

// class AuthenticationProvider extends ChangeNotifier {
//   Widget _homeWidget = const AppWalkThroughWidget();
//   int _homeIndex = 0;
//
//   setAppHomeWidget(AppHomeWidgets widget)  {
//     switch(widget) {
//       case AppHomeWidgets.tabBar:
//         _homeIndex = 2;
//         _homeWidget = const MainTabBarWidget();
//         break;
//       case AppHomeWidgets.authWrapper:
//         _homeIndex = 1;
//         _homeWidget = const AuthenticationWrapperWidget();
//         break;
//       default:
//         _homeIndex = 0;
//         _homeWidget = const AppWalkThroughWidget();
//     }
//     notifyListeners();
//   }
//
//   Widget getAppHomeWidget()  {
//     debugPrint('$_homeWidget');
//     return _homeWidget;
//   }
//
//   String getHomeIndex()  {
//     debugPrint('key$_homeIndex');
//     return 'key$_homeIndex';
//   }
// }
