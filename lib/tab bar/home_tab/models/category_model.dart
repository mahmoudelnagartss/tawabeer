class CategoryModel {
  String id;
  String image;
  String title;

  CategoryModel({
    required this.id,
    required this.title,
    required this.image,
  });
}
