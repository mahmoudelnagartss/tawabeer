class CompanyModel {
  String id;
  String image;
  String title;
  String location;
  int rate;
  bool isFavorite;

  CompanyModel({
    required this.id,
    required this.title,
    required this.image,
    required this.location,
    required this.rate,
    required this.isFavorite,
  });
}
