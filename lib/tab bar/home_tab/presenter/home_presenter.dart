import 'package:flutter/cupertino.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/category_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/offer_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePresenter {
  List<CategoryModel> getAllCategories(BuildContext context) {
    List<CategoryModel> list = [];

    for (int index = 0; index < 8; index++) {
      String title = AppLocalizations.of(context)!.categoryTitle;
      String image = 'assets/photo_food_$index.jpeg';

      CategoryModel model = CategoryModel(
        title: title,
        image: image,
        id: '$index',
      );

      list.add(model);
    }

    return list;
  }

  List<CompanyModel> getBestRecommendations(BuildContext context) {
    List<CompanyModel> list = [];

    for (int index = 0; index < 8; index++) {
      String title = AppLocalizations.of(context)!.companyTitle;
      String image = 'assets/photo_food_$index.jpeg';

      CompanyModel model = CompanyModel(
        title: title,
        image: image,
        id: '$index',
        location: AppLocalizations.of(context)!.companyLocation,
        rate: 4,
        isFavorite: (index % 2) == 0,
      );

      list.add(model);
    }

    return list;
  }

  List<OfferModel> getAllOffers() {
    List<OfferModel> list = [];

    for (int index = 0; index < 8; index++) {
      String image = 'assets/photo_food_$index.jpeg';

      OfferModel model = OfferModel(
        image: image,
      );

      list.add(model);
    }

    return list;
  }
}
