import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_widgets/companies_filters_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_widgets/companies_gridview_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_widgets/companies_sortby_widget.dart';
import 'package:tawabeer/tab%20bar/search/search_top_bar_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CompaniesListWidget extends StatefulWidget {
  const CompaniesListWidget({Key? key}) : super(key: key);

  @override
  State<CompaniesListWidget> createState() => _CompaniesListWidgetState();
}

class _CompaniesListWidgetState extends State<CompaniesListWidget> {
  final controller = ScrollController();
  final double appBarExpandedHeight = 200.0;
  final double appBarCollapsedHeight = 80.0;

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: NestedScrollView(
          controller: controller,
          floatHeaderSlivers: false,
          scrollDirection: Axis.vertical,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: SliverSafeArea(
                  top: false,
                  bottom: false,
                  sliver: SliverAppBar(
                    expandedHeight: appBarExpandedHeight,
                    collapsedHeight: appBarCollapsedHeight,
                    centerTitle: false,
                    floating: true,
                    pinned: true,
                    snap: false,
                    primary: true,
                    elevation: 0.0,
                    forceElevated: false,
                    automaticallyImplyLeading: true,
                    flexibleSpace: LayoutBuilder(
                      builder:
                          (BuildContext context, BoxConstraints constraints) {
                        // double percent =
                        //     ((constraints.maxHeight - appBarCollapsedHeight) *
                        //             100) /
                        //         (appBarExpandedHeight - appBarCollapsedHeight) - 39.16;
                        //
                        // debugPrint('percent $percent ');
                        //
                        // if (percent > 100) {
                        //   percent = 1;
                        // } else if (percent < 5) {
                        //   percent = 0;
                        // } else {
                        //   percent = percent / 100;
                        // }
                        return Stack(
                          children: <Widget>[
                            Positioned.fill(
                              child: Container(
                                color: AppColors.primaryColor.shade600,
                                child: Image.asset(
                                  'assets/photo_food_0.jpeg',
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            Positioned(
                              bottom: 0,
                              child: Container(
                                width: _screenSize.width,
                                height: 20,
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20),
                                  ),
                                ),
                              ),
                            ),
                            // Positioned(
                            //   right: ((_screenSize.width - 50.0) / 2),
                            //   bottom: 40,
                            //   child: Container(
                            //     width: 50,
                            //     height: 50,
                            //     decoration: BoxDecoration(
                            //       color: Colors.white.withOpacity(percent),
                            //       borderRadius: const BorderRadius.all(
                            //         Radius.circular(25),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ];
          },
          body: Container(
            color: Colors.white,
            padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.subCategoryTitle,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                    Row(
                      children: [
                        IconButton(
                          onPressed: () {
                            showModalBottomSheet<void>(
                              context: context,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              // backgroundColor: Colors.transparent,
                              builder: (BuildContext context) {
                                return const CompaniesFiltersWidget();
                              },
                            );
                          },
                          icon: Icon(
                            Icons.filter_alt_outlined,
                            size: 24,
                            color: Colors.grey.shade600,
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        IconButton(
                          onPressed: () {
                            showModalBottomSheet<void>(
                              context: context,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              // backgroundColor: Colors.transparent,
                              builder: (BuildContext context) {
                                return const CompaniesSortByWidget();
                              },
                            );
                          },
                          icon: Icon(
                            Icons.sort_by_alpha,
                            size: 24,
                            color: Colors.grey.shade600,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                const SearchTopBarWidget(),
                const SizedBox(
                  height: 8,
                ),
                const CompaniesGridViewWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
