import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/company_card_widget.dart';

class CompaniesGridViewWidget extends StatelessWidget {
  const CompaniesGridViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    String title = AppLocalizations.of(context)!.companyTitle;
    String image = 'assets/photo_food_0.jpeg';

    CompanyModel model = CompanyModel(
      title: title,
      image: image,
      id: '1',
      location: AppLocalizations.of(context)!.companyLocation,
      rate: 4,
      isFavorite: true,
    );

    return Expanded(
      child: StaggeredGridView.countBuilder(
        padding: const EdgeInsets.all(0.0),
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 0.0,
        crossAxisCount: 8,
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) =>
            (((index + 1) % 5 == 0))
                ? Container(
                    decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ))
                : CompanyCardWidget(
                    height: 0,
                    width: 0,
                    companyModel: model,
                  ),
        staggeredTileBuilder: (int index) => ((index + 1) % 5 == 0)
            ? const StaggeredTile.count(8, 2)
            : const StaggeredTile.count(4, 4),
      ),
    );
  }

  getChildren(BuildContext context) {
    List<CompanyCardWidget> list = [];
    String title = AppLocalizations.of(context)!.companyTitle;
    String image = 'assets/photo_food_0.jpeg';

    CompanyModel model = CompanyModel(
      title: title,
      image: image,
      id: '1',
      location: AppLocalizations.of(context)!.companyLocation,
      rate: 4,
      isFavorite: true,
    );

    for (int index = 0; index < 100; index++) {
      list.add(
        CompanyCardWidget(
          height: 0,
          width: 0,
          companyModel: model,
        ),
      );
    }

    return list;
  }
}
