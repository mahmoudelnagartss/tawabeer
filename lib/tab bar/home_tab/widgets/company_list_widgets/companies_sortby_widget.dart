import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CompaniesSortByWidget extends StatefulWidget {
  const CompaniesSortByWidget({Key? key}) : super(key: key);

  @override
  State<CompaniesSortByWidget> createState() => _CompaniesSortByWidgetState();
}

class _CompaniesSortByWidgetState extends State<CompaniesSortByWidget> {
  int selectedValue = 0;

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return TopCorneredContainerWidget(
      height: 400,
      childWidget: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.language,
                  size: 32,
                  color: Colors.grey.shade600,
                ),
                const SizedBox(
                  width: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.sortBy,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.sortByDesc,
            ),
            RadioListTile<int>(
              value: 0,
              groupValue: selectedValue,
              title:
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.bestRated,
              ),
              activeColor: AppColors.primaryColor.shade800,
              onChanged: (value) {
                setState(() {
                  selectedValue = 0;
                });
              },
            ),
            RadioListTile<int>(
              value: 1,
              groupValue: selectedValue,
              title: ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.nearest,
              ),

              activeColor: AppColors.primaryColor.shade800,
              onChanged: (value) {
                setState(() {
                  selectedValue = 1;
                });
              },
            ),
            RadioListTile<int>(
              value: 2,
              groupValue: selectedValue,
              title: ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.newest,
              ),

              activeColor: AppColors.primaryColor.shade800,
              onChanged: (value) {
                setState(() {
                  selectedValue = 2;
                });
              },
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 60,
              width: _screenSize.width,
              child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                  color: AppColors.primaryColor.shade800,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(30.0),
                  ),
                ),
                child: Center(
                  child: TextButton(
                    onPressed: () {},
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.apply,
                      fontWeight: FontWeight.w600,
                      fontSize: 18.0,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
