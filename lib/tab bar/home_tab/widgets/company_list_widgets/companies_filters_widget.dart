import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/drop_down_menu_labeled_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CompaniesFiltersWidget extends StatefulWidget {
  const CompaniesFiltersWidget({Key? key}) : super(key: key);

  @override
  _CompaniesFiltersWidgetState createState() => _CompaniesFiltersWidgetState();
}

class _CompaniesFiltersWidgetState extends State<CompaniesFiltersWidget> {
  int selectedValue = 0;

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return TopCorneredContainerWidget(
      height: 800,
      childWidget: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.language,
                  size: 32,
                  color: Colors.grey.shade600,
                ),
                const SizedBox(
                  width: 20,
                ),

                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.applyFilters,
                  fontWeight: FontWeight.bold,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.filterByDesc,
            ),
            const SizedBox(
              height: 8,
            ),
            DropdownMenuLabeledWidget(
              title: AppLocalizations.of(context)!.rate,
              list: [
                AppLocalizations.of(context)!.more_5Stars,
                AppLocalizations.of(context)!.more_4Stars,
                AppLocalizations.of(context)!.more_3Stars,
                AppLocalizations.of(context)!.more_2Stars,
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            DropdownMenuLabeledWidget(
              title: AppLocalizations.of(context)!.nationality,
              list: [
                AppLocalizations.of(context)!.saudi,
                AppLocalizations.of(context)!.egyptian,
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            DropdownMenuLabeledWidget(
              title: AppLocalizations.of(context)!.price,
              list: [
                AppLocalizations.of(context)!.moreThan1200,
                AppLocalizations.of(context)!.moreThan500,
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 60,
              width: _screenSize.width,
              child: Container(
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                  color: AppColors.primaryColor.shade800,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(30.0),
                  ),
                ),
                child: Center(
                  child: TextButton(
                    onPressed: () {},
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.apply,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
