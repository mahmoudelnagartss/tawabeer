import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_with_category/sub_category_card_widget.dart';
import 'package:avatar_view/avatar_view.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SubCategoryListWidget extends StatelessWidget {
  const SubCategoryListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        appBar: const AppBarWidget(
          barTitle: '',
        ),
        body: Container(
          // color: AppColors.primaryColor.shade600,
          decoration: BoxDecoration(
            color: AppColors.primaryColor.shade600,
            image: const DecorationImage(
              image: AssetImage(
                'assets/bgProduct.png',
              ),
              fit: BoxFit.fitWidth,
              alignment: FractionalOffset.topCenter,
            ),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 0,
                width: _screenSize.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        color: AppColors.primaryColor.shade700,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(75),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 100,
                child: SingleChildScrollView(
                  child: TopCorneredContainerWidget(
                    childWidget: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          child: ListView.builder(
                            padding: const EdgeInsets.all(0.0),
                            scrollDirection: Axis.vertical,
                            itemCount: 10,
                            itemBuilder: (_, int index) {
                              return const SubCategoryCardWidget(
                                height: 150,
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 10,
                width: _screenSize.width - 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    AvatarView(
                      radius: 50,
                      borderColor: AppColors.primaryColor.shade500,
                      borderWidth: 0,
                      isOnlyText: false,
                      avatarType: AvatarType.CIRCLE,
                      backgroundColor: AppColors.primaryColor.shade600,
                      imagePath: 'assets/photo_food_0.jpeg',
                      placeHolder: const Icon(
                        Icons.e_mobiledata_sharp,
                      ),
                      errorWidget: const Icon(
                        Icons.e_mobiledata_sharp,
                      ),
                    ),
                    const SizedBox(
                      width: 60,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.categoryTitle,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: TextColor.whiteColor,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
