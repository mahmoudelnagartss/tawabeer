import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_widgets/companies_list_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SubCategoryCardWidget extends StatelessWidget {
  final double height;

  const SubCategoryCardWidget({Key? key, required this.height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) {
              return const CompaniesListWidget();
            },
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.all(8.0),
        height: height,
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          image: const DecorationImage(
            image: AssetImage(
              'assets/photo_food_0.jpeg',
            ),
            fit: BoxFit.fill,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              bottom: 8,
              left: 16,
              right: 16,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.subCategoryTitle,
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                    color: TextColor.whiteColor,
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(16, 5, 16, 5),
                    decoration: BoxDecoration(
                      color: Colors.grey.shade800,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(16),
                      ),
                    ),
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.companiesCount,
                      fontWeight: FontWeight.w400,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
