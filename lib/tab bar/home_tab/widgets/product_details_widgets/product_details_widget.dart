import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/product_details_widgets/cards/details_add_to_cart_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/product_details_widgets/cards/product_details_header_card.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/product_details_widgets/cards/selection_category_card.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/product_details_widgets/cards/selection_option_card.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProductDetailsWidget extends StatefulWidget {
  const ProductDetailsWidget({Key? key}) : super(key: key);

  @override
  _ProductDetailsWidgetState createState() => _ProductDetailsWidgetState();
}

class _ProductDetailsWidgetState extends State<ProductDetailsWidget> {
  var selectedSize = 0;
  var selectedExtra = 0;

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    List<Widget> getOptionWidgets() {
      List<Widget> list = [];

      list.add(
        const ProductDetailsHeaderCard(),
      );

      list.add(
         SelectionCategoryCard(
          title: AppLocalizations.of(context)!.size,
        ),
      );

      list.add(
        SelectionOptionCard(
          title: AppLocalizations.of(context)!.normal,
          radioGroupValue: selectedSize,
          radioValue: 0,
          returnRadioSelected: (value) {
            setState(() {
              selectedSize = value;
              debugPrint('Value $value');
            });
          },
        ),
      );

      list.add(
        SelectionOptionCard(
          title: AppLocalizations.of(context)!.large,
          radioGroupValue: selectedSize,
          radioValue: 1,
          returnRadioSelected: (value) {
            setState(() {
              selectedSize = value;
              debugPrint('Value $value');
            });
          },
        ),
      );
      list.add(
         SelectionCategoryCard(
          title: AppLocalizations.of(context)!.extra,
        ),
      );

      list.add(
        SelectionOptionCard(
          title: AppLocalizations.of(context)!.coloSlo,
          radioGroupValue: selectedExtra,
          radioValue: 0,
          returnRadioSelected: (value) {
            setState(() {
              selectedExtra = value;
              debugPrint('Value $value');
            });
          },
        ),
      );

      list.add(
        SelectionOptionCard(
          title: AppLocalizations.of(context)!.sosBBQ,
          radioGroupValue: selectedExtra,
          radioValue: 1,
          returnRadioSelected: (value) {
            setState(() {
              selectedExtra = value;
              debugPrint('Value $value');
            });
          },
        ),
      );

      list.add(
        SelectionOptionCard(
          title: AppLocalizations.of(context)!.sosTasty,
          radioGroupValue: selectedExtra,
          radioValue: 2,
          returnRadioSelected: (value) {
            setState(() {
              selectedExtra = value;
              debugPrint('Value $value');
            });
          },
        ),
      );
      list.add(const DetailsAddToCartWidget());

      return list;
    }

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            color: AppColors.primaryColor.shade600,
            image: const DecorationImage(
              image: AssetImage(
                'assets/bgProduct.png',
              ),
              fit: BoxFit.fitWidth,
              alignment: FractionalOffset.topCenter,
            ),
          ),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Positioned(
                top: 50,
                width: _screenSize.width - 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(
                        Icons.arrow_drop_down_circle_outlined,
                        color: Colors.white,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.productDetails,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: TextColor.whiteColor,
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 180,
                bottom: 0,
                child: TopCorneredContainerWidget(
                  childWidget: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 40, 0, 0),
                    child: ListView(
                      children: getOptionWidgets(),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 100,
                width: _screenSize.width - 30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AvatarView(
                      radius: 70,
                      borderColor: AppColors.primaryColor.shade600,
                      borderWidth: 5,
                      isOnlyText: false,
                      avatarType: AvatarType.CIRCLE,
                      backgroundColor: AppColors.primaryColor.shade600,
                      imagePath: 'assets/photo_food_0.jpeg',
                      placeHolder: const Icon(
                        Icons.e_mobiledata_sharp,
                      ),
                      errorWidget: const Icon(
                        Icons.e_mobiledata_sharp,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
