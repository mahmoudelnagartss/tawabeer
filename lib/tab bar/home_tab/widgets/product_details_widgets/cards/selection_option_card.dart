import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SelectionOptionCard extends StatelessWidget {
  final String title;
  final int radioGroupValue;
  final int radioValue;
  final Function returnRadioSelected;

  const SelectionOptionCard(
      {Key? key,
      required this.title,
      required this.radioGroupValue,
      required this.radioValue,
      required this.returnRadioSelected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ColoredTitleTextWidget(
                    text: title,
                    fontSize: 14.0,
                    color: TextColor.blackColor,
                  ),

                  const SizedBox(
                    height: 8.0,
                  ),
                  ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.productPrice,
                    fontSize: 13.0,
                    color: TextColor.appMainColor,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
              Flexible(
                child: SizedBox(
                  width: 80,
                  child: RadioListTile<int>(
                    value: radioValue,
                    groupValue: radioGroupValue,
                    activeColor: AppColors.primaryColor.shade800,
                    onChanged: (value) {
                      returnRadioSelected(value);
                    },
                  ),
                ),
              ),
            ],
          ),
          Container(
            height: 1,
            width: double.infinity,
            color: Colors.grey.shade200,
          )
        ],
      ),
    );
  }
}
