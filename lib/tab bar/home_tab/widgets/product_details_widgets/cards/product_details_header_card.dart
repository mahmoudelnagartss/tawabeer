import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class ProductDetailsHeaderCard extends StatelessWidget {
  const ProductDetailsHeaderCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 110,
      padding: const EdgeInsets.all(
        8.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.productShortDesc,
                  fontWeight: FontWeight.bold,
                  fontSize: 18.0,
                  color: TextColor.blackColor,
                  numberOfLines: 1,
                ),
              ),
              const SizedBox(
                width: 8.0,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.productPrice,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: TextColor.appMainColor,
                numberOfLines: 1,
              ),
            ],
          ),
          const SizedBox(
            height: 8.0,
          ),
          Flexible(
            child: ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.productShortDesc,
              fontSize: 13.0,
              numberOfLines: 1,
            ),
          ),
        ],
      ),
    );
  }
}
