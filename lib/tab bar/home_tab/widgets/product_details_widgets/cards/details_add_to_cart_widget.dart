import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DetailsAddToCartWidget extends StatefulWidget {
  const DetailsAddToCartWidget({Key? key}) : super(key: key);

  @override
  _DetailsAddToCartWidgetState createState() => _DetailsAddToCartWidgetState();
}

class _DetailsAddToCartWidgetState extends State<DetailsAddToCartWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 30, 0, 30),
      height: 80,
      decoration: BoxDecoration(
        color: Colors.grey.shade50,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Card(
        elevation: 5.0,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.grey.shade100,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.addToCart,
                    fontSize: 14.0,
                    numberOfLines: 1,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Row(
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.add_circle,
                      color: AppColors.primaryColor.shade800,
                      size: 24,
                    ),
                    onPressed: () async {
                      debugPrint('plus');
                    },
                  ),
                  const ColoredTitleTextWidget(
                    text: '11',
                    fontSize: 15.0,
                    numberOfLines: 1,
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.remove_circle_outline,
                      color: AppColors.primaryColor.shade500,
                      size: 24,
                    ),
                    onPressed: () async {
                      debugPrint('remove');
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
