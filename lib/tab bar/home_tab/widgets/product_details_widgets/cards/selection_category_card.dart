import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';

class SelectionCategoryCard extends StatelessWidget {
  final String title;

  const SelectionCategoryCard({Key? key, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(24, 8, 24, 8),
          decoration: BoxDecoration(
            color: Colors.grey.shade200,
            borderRadius: const BorderRadius.all(
              Radius.circular(16),
            ),
          ),

          child:
          ColoredTitleTextWidget(
            text: title,
            fontSize: 14.0,
          ),
        )
      ],
    );
  }
}
