import 'package:flutter/material.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/services/page_transition.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_tab_widget.dart';

class CompanyDetailsHeader extends StatelessWidget {
  const CompanyDetailsHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverOverlapAbsorber(
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: SliverSafeArea(
        top: false,
        bottom: false,
        sliver: SliverAppBar(
          // actions: [
          //   IconButton(
          //     onPressed: () {
          //       Navigator.of(context).push(
          //         PageTransition(
          //           child: CartTabWidget(
          //             openOrdersTab: () {
          //
          //             },
          //           ),
          //           transitionType: TransitionTypes.slideFromBottom,
          //         ),
          //       );
          //     },
          //     icon: const Icon(
          //       Icons.shopping_cart,
          //       color: Colors.white,
          //     ),
          //   ),
          //   const SizedBox(
          //     width: 8,
          //   ),
          // ],
          expandedHeight: 150.0,
          collapsedHeight: 60.0,
          centerTitle: false,
          floating: true,
          pinned: true,
          snap: false,
          primary: true,
          elevation: 0.0,
          forceElevated: false,
          automaticallyImplyLeading: true,
          flexibleSpace: Stack(
            children: <Widget>[
              Positioned.fill(
                child: Container(
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    image: const DecorationImage(
                      image: AssetImage(
                        'assets/bgProduct.png',
                      ),
                      fit: BoxFit.fitWidth,
                      alignment: FractionalOffset.topCenter,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
