import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/cards/company_products_widget.dart';

class CompanyInfoWidget extends StatefulWidget {
  const CompanyInfoWidget({Key? key}) : super(key: key);

  @override
  State<CompanyInfoWidget> createState() => _CompanyInfoWidgetState();
}

class _CompanyInfoWidgetState extends State<CompanyInfoWidget> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        AvatarView(
          radius: 45,
          borderColor: AppColors.primaryColor,
          borderWidth: 2,
          isOnlyText: false,
          avatarType: AvatarType.CIRCLE,
          backgroundColor: AppColors.primaryColor.shade600,
          imagePath: "assets/photo_food_0.jpeg",
          placeHolder: const Icon(
            Icons.home_filled,
          ),
          errorWidget: const Icon(
            Icons.home_filled,
          ),
        ),
        const SizedBox(
          height: 2,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.companyTitle,
              fontWeight: FontWeight.w500,
              fontSize: 18.0,
              color: TextColor.blackColor,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.share,
                    color: Colors.grey.shade600,
                    size: 24,
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      isFavorite = !isFavorite;
                    });
                  },
                  icon: isFavorite
                      ? Icon(
                    Icons.favorite,
                    color: AppColors.primaryColor.shade600,
                    size: 24,
                  )
                      : Icon(
                    Icons.favorite_outline_sharp,
                    color: Colors.grey.shade600,
                    size: 24,
                  ),
                ),
              ],
            ),
          ],
        ),
        const SizedBox(
          height: 2,
        ),
        RatingBar(
          updateOnDrag: false,
          initialRating: 3.5,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          ignoreGestures: true,
          // enable and disable
          tapOnlyMode: true,
          itemSize: 18.0,
          ratingWidget: RatingWidget(
            full: const Text(
              '⭐',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
            half: const Text(
              '⭐',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
            empty: const Text(
              '✨',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
              ),
            ),
          ),
          itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
          onRatingUpdate: (rating) {},
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.location_on,
                  color: Colors.grey.shade600,
                  size: 24,
                ),
                const SizedBox(
                  width: 5,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.companyLocation,
                  fontWeight: FontWeight.w400,
                  fontSize: 14.0,
                ),
              ],
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.map_sharp,
                      color: Colors.grey.shade600,
                      size: 24,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.openOnMaps,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        const Expanded(
          child: CompanyProductsWidget(),
        ),
      ],
    );
  }
}
