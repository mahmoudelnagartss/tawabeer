import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/cards/product_card_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CompanyProductsWidget extends StatefulWidget {
  const CompanyProductsWidget({Key? key}) : super(key: key);

  @override
  _CompanyProductsWidgetState createState() => _CompanyProductsWidgetState();
}

class _CompanyProductsWidgetState extends State<CompanyProductsWidget>
    with SingleTickerProviderStateMixin {
  TabController? _tabController;
  List<Widget> tabs = [];
  int selectedTabIndex = 0;
  List<Widget> tabViews = [];

  @override
  void initState() {
    _tabController = TabController(
      length: 10,
      vsync: this,
    );

    _tabController!.addListener(
      () {
        setState(
          () {
            selectedTabIndex = _tabController!.index;
          },
        );
      },
    );

    for (int index = 0; index < 10; index++) {
      tabViews.add(
        ListView.builder(
          key: PageStorageKey('Key$index'),
          padding: const EdgeInsets.all(
            0.0,
          ),
          scrollDirection: Axis.vertical,
          itemCount: 20,
          itemBuilder: (_, int index) {
            return const ProductCardWidget(shouldShowCartIcon: true,);
          },
        ),
      );
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    tabs = [];
    for (int index = 0; index < 10; index++) {
      tabs.add(
        Container(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
          decoration: BoxDecoration(
            color: (index == selectedTabIndex)
                ? Colors.grey.shade800
                : Colors.grey.shade100,
            borderRadius: BorderRadius.circular(15),
          ),
          child: ColoredTitleTextWidget(
            text: AppLocalizations.of(context)!.subCategoryTitle,
            fontWeight: FontWeight.bold,
            fontSize: 12.0,
            color: (index == selectedTabIndex)
                ? TextColor.whiteColor
                : TextColor.greyColor,
          ),
        ),
      );
    }

    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
          child: TabBar(
            controller: _tabController!,
            labelColor: Colors.green,
            isScrollable: true,
            indicatorColor: Colors.transparent,
            unselectedLabelColor: Colors.grey,
            unselectedLabelStyle: const TextStyle(
              fontSize: 16,
              color: Colors.grey,
              fontWeight: FontWeight.w700,
            ),
            labelStyle: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
            tabs: tabs,
            onTap: (value) {
              setState(() {
                selectedTabIndex = value;
              });
            },
          ),
        ),
        Expanded(
          child: TabBarView(
            controller: _tabController!,
            children: tabViews,
          ),
        ),
      ],
    );
  }
}
