import 'package:avatar_view/avatar_view.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/product_details_widgets/product_details_widget.dart';

class ProductCardWidget extends StatefulWidget {
  final bool shouldShowCartIcon;

  const ProductCardWidget({
    Key? key,
    required this.shouldShowCartIcon,
  }) : super(key: key);

  @override
  State<ProductCardWidget> createState() => _ProductCardWidgetState();
}

class _ProductCardWidgetState extends State<ProductCardWidget> {
  bool isCartItem = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).push(
        //   MaterialPageRoute(
        //     builder: (BuildContext context) {
        //       return const ProductDetailsWidget();
        //     },
        //     fullscreenDialog: true,
        //   ),
        // );

        // Navigator.of(context).push(
        //   PageTransition(
        //     child: const ProductDetailsWidget(),
        //     transitionType: TransitionTypes.slideFromRight,
        //   ),
        // );

        showModalBottomSheet<void>(
          isScrollControlled: true,
          context: context,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          // backgroundColor: Colors.transparent,
          builder: (BuildContext context) {
            return const ProductDetailsWidget();
          },
        );
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Row(
          children: [
            AvatarView(
              radius: 40,
              borderColor: Colors.grey.shade400,
              borderWidth: 2,
              isOnlyText: false,
              avatarType: AvatarType.CIRCLE,
              backgroundColor: AppColors.primaryColor.shade600,
              imagePath: 'assets/photo_food_0.jpeg',
              placeHolder: const Icon(
                Icons.e_mobiledata_sharp,
              ),
              errorWidget: const Icon(
                Icons.e_mobiledata_sharp,
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            Flexible(
              child: Card(
                color: Colors.grey.shade200,
                elevation: 0.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.productTitle,
                      fontWeight: FontWeight.w600,
                      fontSize: 14.0,
                      color: TextColor.blackColor,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.productShortDesc,
                      fontSize: 12.0,
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.productPrice,
                          fontWeight: FontWeight.w600,
                          fontSize: 14.0,
                          color: TextColor.appMainColor,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        (widget.shouldShowCartIcon
                            ? IconButton(
                                onPressed: () {
                                  setState(() {
                                    isCartItem = !isCartItem;
                                  });
                                },
                                icon: isCartItem
                                    ? Icon(
                                        Icons.shopping_cart,
                                        color: AppColors.primaryColor.shade600,
                                        size: 24,
                                      )
                                    : Icon(
                                        Icons.add_shopping_cart_sharp,
                                        color: Colors.grey.shade600,
                                        size: 24,
                                      ),
                              )
                            : const SizedBox.shrink()),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
