import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/cards/company_details_header.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/cards/company_info_widgets.dart';

class CompanyDetailsWidget extends StatefulWidget {
  const CompanyDetailsWidget({Key? key}) : super(key: key);

  @override
  _CompanyDetailsWidgetState createState() => _CompanyDetailsWidgetState();
}

class _CompanyDetailsWidgetState extends State<CompanyDetailsWidget> {
  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: NestedScrollView(
          floatHeaderSlivers: false,
          scrollDirection: Axis.vertical,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              const CompanyDetailsHeader(),
            ];
          },
          body: Container(
            color: AppColors.primaryColor.shade600,
            child: const TopCorneredContainerWidget(
              childWidget: CompanyInfoWidget(),
            ),
          ),
        ),
      ),
    );
  }
}
