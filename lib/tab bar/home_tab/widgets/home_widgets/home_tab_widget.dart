import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/category_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/offer_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/presenter/home_presenter.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/RecommendationsListWidget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/category_card_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/home_header_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/offer_card_widget.dart';
import 'package:tawabeer/tab%20bar/search/search_top_bar_widget.dart';

class HomeTabWidget extends StatefulWidget {
  const HomeTabWidget({Key? key}) : super(key: key);

  @override
  _HomeTabWidgetState createState() => _HomeTabWidgetState();
}

class _HomeTabWidgetState extends State<HomeTabWidget> {
  final List<OfferModel> _offers = HomePresenter().getAllOffers();
  int _current = 0;
  final CarouselController _controller = CarouselController();

  final PageStorageKey _mainListViewPageStorageKey =
      const PageStorageKey('MainListViewPageStorageKey');
  final PageStorageKey _offersPageStorageKey =
      const PageStorageKey('offersPageStorageKey');
  final PageStorageKey _categoriesPageStorageKey =
      const PageStorageKey('categoriesPageStorageKey');

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    final List<CategoryModel> _categories =
        HomePresenter().getAllCategories(context);

    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: NestedScrollView(
          floatHeaderSlivers: false,
          scrollDirection: Axis.vertical,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              const HomeHeaderWidget(),
            ];
          },
          body: Container(
            color: AppColors.primaryColor.shade600,
            child: TopCorneredContainerWidget(
              childWidget: Column(
                children: [
                  const SearchTopBarWidget(),
                  Expanded(
                    child: ListView(
                      key: _mainListViewPageStorageKey,
                      padding: const EdgeInsets.all(0.0),
                      children: [
                        CarouselSlider(
                          key: _offersPageStorageKey,
                          carouselController: _controller,
                          options: CarouselOptions(
                            autoPlay: true,
                            autoPlayAnimationDuration:
                                const Duration(milliseconds: 200),
                            enlargeCenterPage: true,
                            scrollDirection: Axis.horizontal,
                            viewportFraction: 0.7,
                            // aspectRatio: 1.0,
                            initialPage: 0,
                            height: 150.0,
                            onPageChanged: (index, reason) {
                              setState(() {
                                _current = index;
                              });
                            },
                          ),
                          items: _offers
                              .map(
                                (offer) => OfferCardWidget(
                                    width: _screenSize.width - 50,
                                    offerModel: offer),
                              )
                              .toList(),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: _offers.asMap().entries.map(
                            (entry) {
                              return GestureDetector(
                                onTap: () =>
                                    _controller.animateToPage(entry.key),
                                child: Container(
                                  width: 8.0,
                                  height: 8.0,
                                  margin: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 4.0),
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: (_current == entry.key)
                                        ? Colors.grey.shade600
                                        : Colors.grey.shade300,
                                  ),
                                ),
                              );
                            },
                          ).toList(),
                        ),
                        ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.categories,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          height: 130,
                          child: ListView.builder(
                            key: _categoriesPageStorageKey,
                            scrollDirection: Axis.horizontal,
                            itemCount: _categories.length,
                            itemBuilder: (_, int index) {
                              return CategoryCardWidget(
                                width: _screenSize.width * 0.6,
                                categoryModel: _categories[index],
                              );
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 16,
                        ),
                        ColoredTitleTextWidget(
                          text: AppLocalizations.of(context)!.ourChoice,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        const RecommendationsListWidget(),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

// Scrollbar(
// hoverThickness: 10,
// interactive: true,
// isAlwaysShown: true,
// showTrackOnHover: true,
// radius: const Radius.circular(20),
// thickness: 8,
// controller: _offersScrollController,
// trackVisibility: true,
// child: ListView.builder(
// controller: _offersScrollController,
// scrollDirection: Axis.horizontal,
// itemCount: _offers.length,
// itemBuilder: (_, int index) {
// return OfferCardWidget(
// width: _screenSize.width - 50,
// offerModel: _offers[index],
// );
// }),
// ),
