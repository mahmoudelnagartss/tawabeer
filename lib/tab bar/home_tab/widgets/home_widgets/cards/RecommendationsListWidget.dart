import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/company_card_widget.dart';

class RecommendationsListWidget extends StatelessWidget {
  const RecommendationsListWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    String title = AppLocalizations.of(context)!.companyTitle;
    String image = 'assets/photo_food_0.jpeg';

    CompanyModel model = CompanyModel(
      title: title,
      image: image,
      id: '1',
      location: AppLocalizations.of(context)!.companyLocation,
      rate: 4,
      isFavorite: true,
    );

    double height = ((_screenWidth / 2) + 28)/*height for each item*/ * (20 /*items count*/ / 2) ;

    return SizedBox(
      height: height,
      child: StaggeredGridView.countBuilder(
        physics: const NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.all(0.0),
        mainAxisSpacing: 8.0,
        crossAxisSpacing: 0.0,
        crossAxisCount: 8,
        itemCount: 20,
        itemBuilder: (BuildContext context, int index) => CompanyCardWidget(
          height: 0,
          width: 0,
          companyModel: model,
        ),
        staggeredTileBuilder: (int index) => const StaggeredTile.count(4, 4),
      ),
    );
  }

  getChildren(BuildContext context) {
    List<CompanyCardWidget> list = [];
    String title = AppLocalizations.of(context)!.companyTitle;
    String image = 'assets/photo_food_0.jpeg';

    CompanyModel model = CompanyModel(
      title: title,
      image: image,
      id: '1',
      location: AppLocalizations.of(context)!.companyLocation,
      rate: 4,
      isFavorite: true,
    );

    for (int index = 0; index < 100; index++) {
      list.add(
        CompanyCardWidget(
          height: 110,
          width: 200,
          companyModel: model,
        ),
      );
    }

    return list;
  }
}
