import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/notifications/notifications_list_widget.dart';
import 'package:flutter/cupertino.dart';


class HomeHeaderWidget extends StatelessWidget {
  const HomeHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverOverlapAbsorber(
      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
      sliver: SliverSafeArea(
        top: false,
        bottom: false,
        sliver: SliverAppBar(
          expandedHeight: 190.0,
          collapsedHeight: 110.0,
          centerTitle: false,
          floating: true,
          pinned: true,
          snap: false,
          primary: true,
          elevation: 0.0,
          forceElevated: false,
          automaticallyImplyLeading: true,
          flexibleSpace: Container(
            color: AppColors.primaryColor.shade600,
            child: SafeArea(
              child: Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.primaryColor.shade600,
                        image: const DecorationImage(
                          image: AssetImage(
                            'assets/bgProduct.png',
                          ),
                          fit: BoxFit.fitWidth,
                          alignment: FractionalOffset.topCenter,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 16,
                    right: 16,
                    top: 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            AvatarView(
                              radius: 45,
                              borderColor: Colors.white,
                              borderWidth: 5,
                              isOnlyText: false,
                              avatarType: AvatarType.CIRCLE,
                              backgroundColor: AppColors.primaryColor.shade600,
                              imagePath: "assets/photo_food_0.jpeg",
                              placeHolder: const Icon(
                                Icons.home_filled,
                              ),
                              errorWidget: const Icon(
                                Icons.home_filled,
                              ),
                            ),
                            const SizedBox(
                              width: 8,
                            ),
                            ColoredTitleTextWidget(
                              text: AppLocalizations.of(context)!.home,
                              fontWeight: FontWeight.bold,
                              color: TextColor.whiteColor,
                              fontSize: 18.0,
                            ),
                          ],
                        ),
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).push(
                              CupertinoPageRoute(
                                builder: (context) {
                                  return const NotificationListWidget();
                                },
                              ),
                            );
                          },
                          icon: const Icon(
                            Icons.notifications,
                            color: Colors.white,
                            size: 32,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
