import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/category_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_list_with_category/sub_category_list_widget.dart';

class CategoryCardWidget extends StatelessWidget {
  final double width;
  final CategoryModel categoryModel;

  const CategoryCardWidget(
      {Key? key, required this.width, required this.categoryModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) {
              return const SubCategoryListWidget();
            },
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.fromLTRB(0, 2, 8, 2),
        width: width,
        decoration: BoxDecoration(
          color: AppColors.primaryColor.shade700,
          borderRadius: const BorderRadius.all(
            Radius.circular(12),
          ),
          image: const DecorationImage(
            image: AssetImage(
              'assets/bgProduct.png',
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: 8,
              child: Container(
                height: 80,
                width: 80,
                decoration: BoxDecoration(
                  color: AppColors.primaryColor.shade600,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(40),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 12,
              child: AvatarView(
                radius: 38,
                borderColor: AppColors.primaryColor.shade500,
                borderWidth: 2,
                isOnlyText: false,
                avatarType: AvatarType.CIRCLE,
                backgroundColor: AppColors.primaryColor.shade600,
                imagePath: categoryModel.image,
                placeHolder: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
                errorWidget: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
              ),
            ),
            Positioned(
              bottom: 6,
              child: ColoredTitleTextWidget(
                text: categoryModel.title,
                color: TextColor.whiteColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
