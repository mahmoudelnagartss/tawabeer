import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/offer_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/company_details_widget.dart';

class OfferCardWidget extends StatelessWidget {
  final double width;
  final OfferModel offerModel;

  const OfferCardWidget({
    Key? key,
    required this.width,
    required this.offerModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) {
              return const CompanyDetailsWidget();
            },
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.all(8.0),
        // width: width,
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          image: DecorationImage(
            image: AssetImage(
              offerModel.image,
            ),
            fit: BoxFit.fill,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(12),
          ),
        ),
      ),
    );
  }
}
