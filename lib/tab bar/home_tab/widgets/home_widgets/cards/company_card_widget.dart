import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/company_details_widget.dart';
import 'package:flutter/cupertino.dart';


class CompanyCardWidget extends StatefulWidget {
  final double height;
  final double width;
  final CompanyModel companyModel;

  const CompanyCardWidget(
      {Key? key,
      required this.height,
      required this.width,
      required this.companyModel})
      : super(key: key);

  @override
  State<CompanyCardWidget> createState() => _CompanyCardWidgetState();
}

class _CompanyCardWidgetState extends State<CompanyCardWidget> {
  bool isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (context) {
              return const CompanyDetailsWidget();
            },
          ),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.all(4.0),
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
          color: Colors.grey.shade100,
          borderRadius: const BorderRadius.all(
            Radius.circular(12),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              width: 60,
              height: 60,
              child: AvatarView(
                radius: 45,
                borderColor: AppColors.primaryColor.shade100,
                borderWidth: 2,
                isOnlyText: false,
                avatarType: AvatarType.CIRCLE,
                backgroundColor: AppColors.primaryColor.shade600,
                imagePath: widget.companyModel.image,
                placeHolder: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
                errorWidget: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
              ),
            ),
            ColoredTitleTextWidget(
              text: widget.companyModel.title,
              fontWeight: FontWeight.bold,
              fontSize: 13.0,
            ),
            RatingBar(
              updateOnDrag: false,
              initialRating: 3.5,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              ignoreGestures: true,
              // enable and disable
              tapOnlyMode: true,
              itemSize: 18.0,
              ratingWidget: RatingWidget(
                full: const Text(
                  '⭐',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
                half: const Text(
                  '⭐',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
                empty: const Text(
                  '✨',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
              ),
              itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
              onRatingUpdate: (rating) {},
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.location_on,
                      color: Colors.grey.shade600,
                      size: 24,
                    ),
                    const SizedBox(
                      width: 2,
                    ),
                    SizedBox(
                      height: 20,
                      child: ColoredTitleTextWidget(
                        text: widget.companyModel.location,
                        fontWeight: FontWeight.bold,
                        fontSize: 13.0,
                      ),
                    ),
                  ],
                ),
                IconButton(
                  onPressed: () {
                    setState(() {
                      isFavorite = !isFavorite;
                    });
                  },
                  icon: isFavorite
                      ? Icon(
                          Icons.favorite,
                          color: AppColors.primaryColor.shade600,
                          size: 24,
                        )
                      : Icon(
                          Icons.favorite_outline_sharp,
                          color: Colors.grey.shade600,
                          size: 24,
                        ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
