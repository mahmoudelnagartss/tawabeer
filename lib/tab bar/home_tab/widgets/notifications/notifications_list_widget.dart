import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:tawabeer/general_widgets/CustomSliverAppBar.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/notifications/delete_notification_confirmation_wiget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/notifications/notification_card_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NotificationListWidget extends StatefulWidget {
  const NotificationListWidget({Key? key}) : super(key: key);

  @override
  _NotificationListWidgetState createState() => _NotificationListWidgetState();
}

class _NotificationListWidgetState extends State<NotificationListWidget> {
  final controller = ScrollController();
  double appBarHeight = 200.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CustomScrollView(
          controller: controller,
          slivers: [
            CustomSliverAppBar(
              appBarHeight: appBarHeight,
              title: AppLocalizations.of(context)!.notifications,
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Slidable(
                    direction: Axis.horizontal,
                    key: UniqueKey(),
                    startActionPane: ActionPane(
                      // A motion is a widget used to control how the pane animates.
                      motion: const StretchMotion(),
                      dragDismissible: false,
                      // A pane can dismiss the Slidable.
                      dismissible: DismissiblePane(
                        onDismissed: () {},
                      ),
                      // All actions are defined in the children parameter.
                      children: [
                        // A SlidableAction can have an icon and/or a label.
                        SlidableAction(
                          onPressed: (context) {
                            showModalBottomSheet<void>(
                              context: context,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0),
                              ),
                              // backgroundColor: Colors.transparent,
                              builder: (BuildContext context) {
                                return const DeleteNotificationConfirmationWidget();
                              },
                            );
                          },
                          backgroundColor: const Color(0xFFFE4A49),
                          foregroundColor: Colors.white,
                          icon: Icons.delete_rounded,
                          label: AppLocalizations.of(context)!.delete,
                        ),
                      ],
                    ),
                    child: const NotificationCardWidget(),
                  );
                },
                childCount: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
