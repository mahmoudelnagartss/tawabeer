import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DeleteNotificationConfirmationWidget extends StatelessWidget {
  const DeleteNotificationConfirmationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return TopCorneredContainerWidget(
      height: 250,
      childWidget: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.delete,
                  size: 32,
                  color: Colors.red.shade600,
                ),
                const SizedBox(
                  width: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.deleteNotification,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                  color: TextColor.blackColor,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ColoredTitleTextWidget(
              text:
                  AppLocalizations.of(context)!.deleteNotificationConfirmation,
            ),
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              height: 40,
              width: _screenSize.width,
              child: Center(
                child: Container(
                  height: 40,
                  width: _screenSize.width * 0.6,
                  decoration: BoxDecoration(
                    color: Colors.red.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.deleteNotification,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
