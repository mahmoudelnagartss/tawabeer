import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/notifications/notification_details_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';


class NotificationCardWidget extends StatelessWidget {
  const NotificationCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            CupertinoPageRoute(
              maintainState: true,
              builder: (BuildContext context) {
                return NotificationDetailsWidget();
              },
              fullscreenDialog: false,
            ),
          );
        },
        child: Card(
          color: Colors.grey.shade200,
          child: Container(
            width: _screenWidth,
            padding: const EdgeInsets.all(
              16.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.notificationTitle,
                  fontWeight: FontWeight.bold,
                  fontSize: 14,
                ),
                const SizedBox(
                  height: 6,
                ),
                const ColoredTitleTextWidget(
                  text: '22/10/2021 10:30 AM',
                  fontSize: 12,
                  numberOfLines: 1,
                ),
                const SizedBox(
                  height: 16,
                ),

                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.notificationShortDesc,
                  fontSize: 14,
                  numberOfLines: 2,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
