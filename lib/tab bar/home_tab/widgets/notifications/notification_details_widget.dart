import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/CustomSliverAppBar.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NotificationDetailsWidget extends StatelessWidget {
  NotificationDetailsWidget({Key? key}) : super(key: key);

  final controller = ScrollController();
  final double appBarHeight = 200.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CustomScrollView(
          controller: controller,
          slivers: [
            CustomSliverAppBar(
              appBarHeight: appBarHeight,
              title: AppLocalizations.of(context)!.details,
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Card(
                      elevation: 0.0,
                      color: Colors.transparent,
                      child: Padding(
                        padding: const EdgeInsets.all(
                          16.0,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ColoredTitleTextWidget(
                              text: AppLocalizations.of(context)!
                                  .notificationTitle,
                              fontWeight: FontWeight.bold,
                              fontSize: 14,
                            ),
                            const SizedBox(
                              height: 6,
                            ),
                            const ColoredTitleTextWidget(
                              text: '22/10/2021 10:30 AM',
                              fontSize: 12,
                              numberOfLines: 1,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            ColoredTitleTextWidget(
                              text: AppLocalizations.of(context)!
                                  .notificationFullDesc,
                              fontSize: 14,
                              numberOfLines: 100,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                childCount: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
