import 'package:animated_bottom_navigation_bar/animated_bottom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/MapsTabWidget.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';
import 'dart:async';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_tab_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/home_tab_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/orders_tab_widget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/profile_tab_widget.dart';

class AnimatedBottomNavigationBarWidget extends StatefulWidget {
  const AnimatedBottomNavigationBarWidget({Key? key}) : super(key: key);

  @override
  _AnimatedBottomNavigationBarWidgetState createState() =>
      _AnimatedBottomNavigationBarWidgetState();
}

class _AnimatedBottomNavigationBarWidgetState
    extends State<AnimatedBottomNavigationBarWidget>
    with SingleTickerProviderStateMixin {
  var _bottomNavIndex = 0; //default index of a first screen

  late AnimationController _animationController;
  late Animation<double> animation;
  late CurvedAnimation curve;

  final HomeTabWidget _homeTabWidget = const HomeTabWidget(
    key: PageStorageKey('HomeTabWidget'),
  );

  final MapsTabWidget _mapsTabWidget = const MapsTabWidget(
    key: PageStorageKey('MapsTabWidget'),
  );

  final ProfileTabWidget _profileTabWidget = const ProfileTabWidget(
    key: PageStorageKey('ProfileTabWidget'),
  );

  CartTabWidget? _cartTabWidget;

  final iconList = <IconData>[
    Icons.home_filled,
    Icons.map,
    Icons.card_travel,
    Icons.person,
  ];

  @override
  void initState() {
    super.initState();
    final systemTheme = SystemUiOverlayStyle.light.copyWith(
      systemNavigationBarColor: HexColor('#373A36'),
      systemNavigationBarIconBrightness: Brightness.light,
    );
    SystemChrome.setSystemUIOverlayStyle(systemTheme);

    _animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    curve = CurvedAnimation(
      parent: _animationController,
      curve: const Interval(
        0.5,
        1.0,
        curve: Curves.fastOutSlowIn,
      ),
    );
    animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(curve);

    Future.delayed(
      const Duration(seconds: 1),
      () => _animationController.forward(),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _tabBarProvider = Provider.of<TabBarProvider>(context);
    _bottomNavIndex = _tabBarProvider.getSelectedTabIndex();

    return Scaffold(
      body: getSelectedTab(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.primaryColor.shade600,
        onPressed: () {
          debugPrint('index center');
          _cartTabWidget ??= const CartTabWidget();

          Navigator.of(context).push(
            MaterialPageRoute(
              maintainState: true,
              builder: (BuildContext context) {
                return _cartTabWidget!;
              },
              fullscreenDialog: true,
            ),
          );
        },
        child: const Icon(
          Icons.shopping_cart,
          color: Colors.white,
          size: 24,
        ),
      ),
      bottomNavigationBar: AnimatedBottomNavigationBar(
        backgroundColor: Colors.grey.shade800,
        activeColor: AppColors.primaryColor.shade600,
        inactiveColor: Colors.white,
        splashColor: AppColors.primaryColor.shade600,
        splashRadius: 20,
        icons: iconList,
        activeIndex: _bottomNavIndex,
        gapLocation: GapLocation.center,
        gapWidth: 100,
        notchMargin: 3,
        notchSmoothness: NotchSmoothness.sharpEdge,
        // leftCornerRadius: 16,
        // rightCornerRadius: 16,
        elevation: 10,
        onTap: (index) {
          debugPrint('index $index');
          _tabBarProvider.setSelectedTabIndex(index);
        },
        //other params
      ),
    );
  }

  Widget getSelectedTab() {
    Widget bodyWidget = Center(
      child: Icon(
        iconList[_bottomNavIndex],
        color: Colors.black,
        size: 140,
      ),
    );

    if (_bottomNavIndex == 0) {
      // _homeTabWidget ??= const HomeTabWidget(
      //   key: PageStorageKey<String>('HomeTabWidget'),
      // );
      bodyWidget = _homeTabWidget;
    } else if (_bottomNavIndex == 1) {
      // _mapsTabWidget ??= const MapsTabWidget(
      //   key: PageStorageKey<String>('MapsTabWidget'),
      // );
      bodyWidget = _mapsTabWidget;
    } else if (_bottomNavIndex == 2) {
      final OrdersTabWidget _ordersTabWidget = OrdersTabWidget(
        // key: const PageStorageKey<String>('OrdersTabWidget'),
        buildContext: context,
      );
      debugPrint('_ordersTabWidget value $_ordersTabWidget');
      // _ordersTabWidget ??= OrdersTabWidget(
      //   key: const PageStorageKey<String>('OrdersTabWidget'),
      //   buildContext: context,
      // );
      bodyWidget = _ordersTabWidget;
    } else if (_bottomNavIndex == 3) {
      // _profileTabWidget ??= const ProfileTabWidget(
      //   key: PageStorageKey<String>('ProfileTabWidget'),
      // );
      bodyWidget = _profileTabWidget;
    }

    return bodyWidget;
  }
}
