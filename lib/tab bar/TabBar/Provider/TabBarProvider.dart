
import 'package:flutter/cupertino.dart';

enum TabNames {
  home,
  maps,
  cart,
  orders,
  profile,
}

class TabBarProvider extends ChangeNotifier {
  int _selectedTabIndex = 0;

  setSelectedTabName(TabNames name)  {
    switch(name) {
      case TabNames.maps:
        _selectedTabIndex = 1;
        break;
      case TabNames.cart:
        _selectedTabIndex = 2;
        break;
      case TabNames.orders:
        _selectedTabIndex = 3;
        break;
      case TabNames.profile:
        _selectedTabIndex = 4;
        break;
      default:
        _selectedTabIndex = 0;
    }
    notifyListeners();
  }

  setSelectedTabIndex(int index)  {
    _selectedTabIndex = index;
    notifyListeners();
  }

  int getSelectedTabIndex()  {
    return _selectedTabIndex;
  }
}
