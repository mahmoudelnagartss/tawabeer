
import 'package:flutter/material.dart';
import 'package:tawabeer/tab%20bar/TabBar/NativeTabBarWidget.dart';
import 'package:tawabeer/tab%20bar/TabBar/AnimatedBottomNavigationBarWidget.dart';

class TabBarWrapper extends StatelessWidget {
  
  final bool showBottomNavigationBar = false;
  const TabBarWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (showBottomNavigationBar) {
      return const AnimatedBottomNavigationBarWidget();
    } else {
      return const NativeTabBarWidget();
    }
  }
}

