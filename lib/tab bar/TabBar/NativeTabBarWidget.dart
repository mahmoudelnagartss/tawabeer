import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/MapsTabWidget.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_tab_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/home_tab_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/orders_tab_widget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/profile_tab_widget.dart';

class NativeTabBarWidget extends StatefulWidget {
  const NativeTabBarWidget({Key? key}) : super(key: key);

  @override
  _NativeTabBarWidgetState createState() => _NativeTabBarWidgetState();
}

class _NativeTabBarWidgetState extends State<NativeTabBarWidget> {
  final CupertinoTabController _cupertinoTabController = CupertinoTabController();

  @override
  Widget build(BuildContext context) {
    return Consumer<TabBarProvider>(builder: (context, _tabBarProvider, child) {
      _cupertinoTabController.index = _tabBarProvider.getSelectedTabIndex();

      return CupertinoTabScaffold(
        controller: _cupertinoTabController,
        tabBar: CupertinoTabBar(
          backgroundColor: Colors.white,
          activeColor: AppColors.primaryColor.shade600,
          inactiveColor: Colors.grey,
          iconSize: 20,
          onTap: (index) {
            debugPrint('selectedWidgetIndex ++= $index');
            _tabBarProvider.setSelectedTabIndex(index);
          },
          items: [
            BottomNavigationBarItem(
              label: AppLocalizations.of(context)!.home,
              icon: const Icon(
                CupertinoIcons.home,
              ),
            ),
            BottomNavigationBarItem(
              label: AppLocalizations.of(context)!.maps,
              icon: const Icon(
                CupertinoIcons.map_fill,
              ),
            ),
            BottomNavigationBarItem(
              label: AppLocalizations.of(context)!.cart,
              icon: const Icon(
                CupertinoIcons.cart,
              ),
            ),
            BottomNavigationBarItem(
              label: AppLocalizations.of(context)!.orders,
              icon: const FaIcon(
                FontAwesomeIcons.shoppingCart,
              ),
            ),
            BottomNavigationBarItem(
              label: AppLocalizations.of(context)!.profile,
              icon: const Icon(
                CupertinoIcons.profile_circled,
              ),
            ),
          ],
        ),
        tabBuilder: (context, index) {
          debugPrint('tabBuilder ++= $index');
          switch (index) {
            case 0:
              return CupertinoTabView(
                builder: (BuildContext context) {
                  return const HomeTabWidget();
                },
              );
            case 1:
              return CupertinoTabView(
                builder: (BuildContext context) {
                  return const MapsTabWidget();
                },
              );
            case 2:
              return CupertinoTabView(
                builder: (BuildContext context) {
                  return const CartTabWidget();
                },
              );
            case 3:
              return CupertinoTabView(
                builder: (BuildContext context) {
                  return OrdersTabWidget(
                    buildContext: context,
                  );
                },
              );
            case 4:
              return CupertinoTabView(
                builder: (BuildContext context) {
                  return const ProfileTabWidget();
                },
              );
            default:
              return Center(
                child: Text(
                  'Content of tab ${index + 1}: News',
                  style: const TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 20.0,
                  ),
                ),
              );
          }
        },
      );
    });
  }
}
