
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/MarkerDetailsWidget.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Models/VendorModel.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Presenter/MapsPresenter.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Presenter/MarkerGenerator.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Presenter/MarkerPresenter.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Widgets/MarkerWidget.dart';
import '../../services/location_services.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MapsTabWidget extends StatefulWidget {
  const MapsTabWidget({Key? key}) : super(key: key);

  @override
  _MapsTabWidgetState createState() => _MapsTabWidgetState();
}

class _MapsTabWidgetState extends State<MapsTabWidget> {
  final Completer<GoogleMapController> _controller = Completer();
  final LocationServices _locationServices = LocationServices();
  late GoogleMapController _googleMapController;
  late String _mapStyle;

  final MapsPresenter _mapsPresenter = MapsPresenter();
  List<VendorModel> vendorModels = [];
  List<Marker> mapAllMarkers = [];
  List<Marker> mapFilteredMarkers = [];

  @override
  void initState() {
    super.initState();

    rootBundle.loadString('assets/map_style.txt').then((string) {
      _mapStyle = string;
      // debugPrint('_mapStyle $_mapStyle');
      getCurrentPosition();
      loadVendorsList();
    });

    Future.delayed(
      const Duration(
        seconds: 2,
      ),
      () {
        debugPrint('Future.delayed');
        return setState(() {});
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    dynamic params = ModalRoute.of(context)!.settings.arguments;
    debugPrint('$params');
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            myLocationEnabled: true,
            myLocationButtonEnabled: true,
            onTap: (latLong) => FocusScope.of(context).requestFocus(
              FocusNode(),
            ),
            initialCameraPosition: CameraPosition(
              target: _locationServices.getDefaultLocation(),
              zoom: 15,
            ),
            onMapCreated: (GoogleMapController controller) {
              // controller.setMapStyle(_mapStyle);
              _controller.complete(controller);
            },
            markers: mapFilteredMarkers.toSet(),
          ),
          Positioned(
            top: 60,
            left: 10,
            right: 10,
            child: Card(
              elevation: 2.0,
              child: SizedBox(
                height: 50,
                child: TextField(
                  onChanged: (text) {
                    filterMarkers(text);
                  },
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  cursorColor: AppColors.primaryColor.shade600,
                  expands: true,
                  maxLines: null,
                  minLines: null,
                  style: TextStyle(
                    fontFamily: AppLocalizations.of(context)!.fontName,
                    fontSize: 13,
                    color: Colors.grey.shade800,
                  ),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(
                      fontFamily: AppLocalizations.of(context)!.fontName,
                    ),
                    labelStyle: TextStyle(
                      fontFamily: AppLocalizations.of(context)!.fontName,
                    ),
                    hintText: AppLocalizations.of(context)!.searchText,
                    focusColor: Colors.brown,
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                        width: 1.0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: AppColors.primaryColor.shade300,
                        width: 1.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void filterMarkers(String searchText) {
    if (searchText.isEmpty) {
      setState(() {
        mapFilteredMarkers = mapAllMarkers;
      });
    } else {
      List<VendorModel> filteredVendors = vendorModels
          .where((vendor) =>
              vendor.title.toLowerCase().contains(searchText.toLowerCase()))
          .toList();

      setState(() {
        mapFilteredMarkers = mapAllMarkers
            .where((marker) => filteredVendors
                .where((vendor) => (marker.markerId.value.toLowerCase() ==
                    vendor.id.toLowerCase()))
                .isNotEmpty)
            .toList();
      });
    }

    updateCameraPosition();
  }

  getCurrentPosition() async {
    _googleMapController = await _controller.future;
    _googleMapController.setMapStyle(_mapStyle);

    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then(
      (_currentLocation) {
        _googleMapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target:
                  LatLng(_currentLocation.latitude, _currentLocation.longitude),
              zoom: 15,
            ),
          ),
        );
      },
    );

    LatLng _cameraTarget = await _locationServices.getCurrentLocation();

    _googleMapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: _cameraTarget,
          zoom: 15,
        ),
      ),
    );
  }

  void loadVendorsList() async {
    vendorModels = await _mapsPresenter.getNearbyVendors();
    debugPrint('get vendorModels: ${vendorModels.length}');
    List<Widget> _widgets = markerWidgets();
    debugPrint('get widgets: ${_widgets.length}');

    MarkerGenerator(_widgets, (bitmaps) async {
      debugPrint('get bitmaps   : ${bitmaps.length}');
      setState(() {
        mapAllMarkers = mapBitmapsToMarkers(bitmaps);
        mapFilteredMarkers = mapAllMarkers;
      });

      updateCameraPosition();
    }).generate(context);
  }

  List<Marker> mapBitmapsToMarkers(List<Uint8List> bitmaps) {
    List<Marker> markersList = [];
    bitmaps.asMap().forEach(
      (index, bmp) {
        final VendorModel vendorModel = vendorModels[index];
        markersList.add(
          Marker(
            markerId: MarkerId(vendorModel.id),
            position: LatLng(
              vendorModel.latitude,
              vendorModel.longitude,
            ),
            icon: BitmapDescriptor.fromBytes(bmp),
            // infoWindow: InfoWindow(
            //   title: vendorModel.title,
            //   snippet: vendorModel.title,
            //   onTap: () {
            //     showDetails(context);
            //   },
            // ),
            onTap: () {
              showDetails(context);
            },
          ),
        );
      },
    );

    return markersList;
  }

  List<Widget> markerWidgets() {
    return vendorModels
        .map((vendor) => MarkerWidget(
              title: vendor.title,
              iconData: Icons.location_on_outlined,
            ))
        .toList();
  }

  showDetails(BuildContext context) {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      // backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return const MarkerDetailsWidget();
      },
    );
  }

  void updateCameraPosition() {
    MarkerPresenter().updateMapToBounds(mapFilteredMarkers, (newPosition) {
      _googleMapController.animateCamera(
        CameraUpdate.newCameraPosition(
          newPosition,
        ),
      );
    });
  }

// List<Marker> getAllMarkers(BuildContext context) {
//   List<Marker> markers = [];
//
//   // return markers;
//   for (int index = 0; index < vendorModels.length; index++) {
//     final MarkerId _markerId = MarkerId(vendorModels[index].id);
//     final Marker _marker = Marker(
//       markerId: _markerId,
//       position: LatLng(
//         vendorModels[index].latitude,
//         vendorModels[index].longitude,
//       ),
//       infoWindow: InfoWindow(
//         title: vendorModels[index].title,
//         snippet: vendorModels[index].title,
//         onTap: () {
//           showDetails(context);
//         },
//       ),
//       icon: BitmapDescriptor.defaultMarker,
//       onTap: () {
//         // showDetails(context);
//       },
//     );
//     markers.add(_marker);
//   }
//
//   return markers;
// }
}
