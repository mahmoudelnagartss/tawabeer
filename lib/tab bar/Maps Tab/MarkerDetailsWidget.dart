import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/company_details_widget.dart';

class MarkerDetailsWidget extends StatelessWidget {
  const MarkerDetailsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return TopCorneredContainerWidget(
      height: 300,
      childWidget: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AvatarView(
                  radius: 40,
                  borderColor: Colors.grey.shade400,
                  borderWidth: 2,
                  isOnlyText: false,
                  avatarType: AvatarType.CIRCLE,
                  backgroundColor: AppColors.primaryColor.shade600,
                  imagePath: 'assets/photo_food_0.jpeg',
                  placeHolder: const Icon(
                    Icons.e_mobiledata_sharp,
                  ),
                  errorWidget: const Icon(
                    Icons.e_mobiledata_sharp,
                  ),
                ),
                Container(
                  height: 40,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 8.0,
                  ),
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        CupertinoPageRoute(
                          builder: (context) {
                            return const CompanyDetailsWidget();
                          },
                        ),
                      );
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.moreDetails,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.companyTitle,
                  fontWeight: FontWeight.bold,
                  color: TextColor.blackColor,
                ),
              ],
            ),
            const SizedBox(
              height: 16,
            ),
            RatingBar(
              updateOnDrag: false,
              initialRating: 3.5,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              ignoreGestures: true,
              // enable and disable
              tapOnlyMode: true,
              itemSize: 18.0,
              ratingWidget: RatingWidget(
                full: const Text(
                  '⭐',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
                half: const Text(
                  '⭐',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
                empty: const Text(
                  '✨',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                  ),
                ),
              ),
              itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
              onRatingUpdate: (rating) {},
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.location_on,
                      color: Colors.grey.shade600,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.companyLocation,
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.social_distance_sharp,
                      color: Colors.grey.shade600,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    const ColoredTitleTextWidget(
                      text: '4 KM',
                      fontWeight: FontWeight.bold,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
