import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class MarkerWidget extends StatelessWidget {
  final String title;
  final IconData iconData;

  const MarkerWidget({Key? key, required this.title, required this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: AppColors.primaryColor.shade800, width: 1),
        color: AppColors.primaryColor.shade600,
        shape: BoxShape.rectangle,
      ),
      child: FittedBox(
        fit: BoxFit.fill,
        child: Row(
          children: [
            ColoredTitleTextWidget(
              text: title,
              numberOfLines: 3,
              color: TextColor.whiteColor,
            ),
            const SizedBox(
              width: 4,
            ),
            Icon(
              iconData,
              size: 32,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
