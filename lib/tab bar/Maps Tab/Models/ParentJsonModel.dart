
import 'package:flutter/cupertino.dart';

class ParentJsonModel {

  late final String status;
  late final String message;
  late final List<dynamic> data;

  ParentJsonModel.fromJson(Map<String, dynamic> json) {

    status = json['status'] ?? '';
    message = json['message'] ?? '';
    data = json['data'];
  }
}