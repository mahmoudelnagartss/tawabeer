
import 'package:flutter/cupertino.dart';

class VendorModel {

  late final String id;
  late final String phone;
  late final String websiteURL;
  late final String image;
  late final String title;
  late final String mainCategoryId;
  late final String location;
  late final bool isActive;
  late final bool isFavorite;
  late final double rate;
  late final double longitude;
  late final double latitude;

  VendorModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    phone = json['phone'];
    websiteURL = json['website_url'];
    image = json['image'];
    title = json['title'];
    mainCategoryId = json['main_category_id'];
    location = json['location'];
    isActive = json['isActive'];
    isFavorite = json['isFavorite'];
    rate = json['rate'];
    longitude = double.parse(json['longitude'] ?? '');
    latitude = double.parse(json['latitude']);
  }
}