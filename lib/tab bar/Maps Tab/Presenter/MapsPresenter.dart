import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Models/ParentJsonModel.dart';
import 'package:tawabeer/tab%20bar/Maps%20Tab/Models/VendorModel.dart';

class MapsPresenter {
  List<VendorModel> vendors = [];
  BitmapDescriptor? customIcon;

  double _randomDouble(double min, double max) {
    return Random().nextDouble() + min;
  }

  Future<List<VendorModel>> getNearbyVendors() async {
    BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(
        size: Size(12, 12),
      ),
      'assets/cart.png',
    ).then((icon) {
      customIcon = icon;
    });

    // Map<String, String> headers = {};
    // header["content-type"] =  "application/x-www-form-urlencoded";
    // headers["x-uqu-auth"] = "b45f76c9fe68653c290ec8049fed5c5e";

    var uri = Uri.parse(
        'http://192.168.100.44:8084/get-json?fileName=Maps-tab/Nearby-Vendors.json');
    debugPrint('get NearbyVendors uri: $uri');

    Response response = await get(uri);
    Map<String, dynamic> result = jsonDecode(response.body);
    ParentJsonModel parentJsonModel = ParentJsonModel.fromJson(result);
    debugPrint('get NearbyVendors: $parentJsonModel');

    if (parentJsonModel.status == 'success') {
      List<VendorModel> vendorsModels =
          parentJsonModel.data.map((e) => VendorModel.fromJson(e)).toList();
      vendors = vendorsModels;
    }
    return vendors;
  }
}




