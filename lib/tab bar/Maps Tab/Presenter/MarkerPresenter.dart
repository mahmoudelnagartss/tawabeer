import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math';

class MarkerPresenter {

  void updateMapToBounds(
      List<Marker> markers, Function returnNewCameraPosition) {
    Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then(
      (_currentLocation) {
        if (markers.isEmpty) {
          returnNewCameraPosition(
            CameraPosition(
              target:
                  LatLng(_currentLocation.latitude, _currentLocation.longitude),
              zoom: 16,
            ),
          );
        } else {
          LatLngBounds bounds = getBounds(markers);

          LatLng centerLatLng =
              LatLng(_currentLocation.latitude, _currentLocation.longitude);
          double zoom = 15;
          debugPrint('Zoom -- $zoom');
          if (markers.length == 1) {
            zoom = 18;
            debugPrint('Zoom -- $zoom');
            centerLatLng = markers[0].position;
          }

          returnNewCameraPosition(
            CameraPosition(
              target: centerLatLng,
              zoom: zoom,
            ),
          );
        }
      },
    );
  }

  LatLngBounds getBounds(List<Marker> markers) {
    final List<double> lngs =
        markers.map<double>((m) => m.position.longitude).toList();
    final List<double> lats =
        markers.map<double>((m) => m.position.latitude).toList();

    final double highestLat = lats.reduce(max);
    final double highestLng = lngs.reduce(max);
    final double lowestLat = lats.reduce(min);
    final double lowestLng = lngs.reduce(min);

    final LatLngBounds bounds = LatLngBounds(
      northeast: LatLng(highestLat, highestLng),
      southwest: LatLng(lowestLat, lowestLng),
    );

    return bounds;
  }

  LatLng getCentralLatlng(List<LatLng> geoCoordinates) {
    if (geoCoordinates.length == 1) {
      return geoCoordinates.first;
    }

    double x = 0;
    double y = 0;
    double z = 0;

    for (var geoCoordinate in geoCoordinates) {
      var latitude = geoCoordinate.latitude * pi / 180;
      var longitude = geoCoordinate.longitude * pi / 180;

      x += cos(latitude) * cos(longitude);
      y += cos(latitude) * sin(longitude);
      z += sin(latitude);
    }

    var total = geoCoordinates.length;

    x = x / total;
    y = y / total;
    z = z / total;

    var centralLongitude = atan2(y, x);
    var centralSquareRoot = sqrt(x * x + y * y);
    var centralLatitude = atan2(z, centralSquareRoot);

    return LatLng(centralLatitude * 180 / pi, centralLongitude * 180 / pi);
  }
}
