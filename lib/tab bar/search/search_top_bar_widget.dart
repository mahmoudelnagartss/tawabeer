import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/page_transition.dart';
import 'package:tawabeer/tab%20bar/search/barcode_search_widget.dart';
import 'package:tawabeer/tab%20bar/search/search_text_widget.dart';

class SearchTopBarWidget extends StatelessWidget {
  const SearchTopBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {
            debugPrint('search text');
            Navigator.of(context).push(
              PageTransition(
                child: const SearchTextWidget(),
                transitionType: TransitionTypes.slideFromBottom,
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.all(5.0),
            margin: const EdgeInsets.all(5.0),
            width: MediaQuery.of(context).size.width - 90,
            height: 40,
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: const BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Center(
              child: Row(
                children: [
                  const SizedBox(
                    width: 8.0,
                  ),
                  Icon(
                    Icons.search,
                    color: Colors.grey.shade600,
                  ),
                  const SizedBox(
                    width: 8.0,
                  ),
                  ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!
                        .searchText,
                    fontSize: 12.0,
                  ),
                ],
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            showModalBottomSheet<void>(
              isScrollControlled: true,
              context: context,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              backgroundColor: Colors.transparent,
              builder: (BuildContext context) {
                return const BarcodeSearchWidget();
              },
            );
          },
          child: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.grey.shade200,
              borderRadius: const BorderRadius.all(
                Radius.circular(20),
              ),
            ),
            child: Icon(
              Icons.qr_code_scanner,
              color: Colors.grey.shade800,
            ),
          ),
        ),
      ],
    );
  }
}
