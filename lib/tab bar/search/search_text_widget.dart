import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SearchTextWidget extends StatefulWidget {
  const SearchTextWidget({Key? key}) : super(key: key);

  @override
  _SearchTextWidgetState createState() => _SearchTextWidgetState();
}

class _SearchTextWidgetState extends State<SearchTextWidget> {
  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.search,
        height: 80,
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(
          FocusNode(),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(16, 5, 16, 5),
                child: TextField(
                  onChanged: (text) async {},
                  keyboardType: const TextInputType.numberWithOptions(),
                  textInputAction: TextInputAction.search,
                  cursorColor: AppColors.primaryColor.shade600,
                  expands: true,
                  maxLines: null,
                  minLines: null,
                  style: TextStyle(
                    fontFamily: AppLocalizations.of(context)!.fontName,
                    fontSize: 13,
                    color: Colors.grey.shade500,
                  ),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(
                      fontFamily: AppLocalizations.of(context)!
                          .fontName,
                    ),
                    labelStyle: TextStyle(
                      fontFamily: AppLocalizations.of(context)!.fontName,
                    ),

                    hintText: AppLocalizations.of(context)!.searchText,
                    focusColor: Colors.brown,
                    filled: true,
                    fillColor: Colors.grey.shade100,
                    contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                        width: 2.0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: AppColors.primaryColor.shade300,
                        width: 0.5,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 300,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
