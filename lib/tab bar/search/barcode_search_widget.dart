import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BarcodeSearchWidget extends StatefulWidget {
  const BarcodeSearchWidget({Key? key}) : super(key: key);

  @override
  State<BarcodeSearchWidget> createState() => _BarcodeSearchWidgetState();
}

class _BarcodeSearchWidgetState extends State<BarcodeSearchWidget> {
  final _searchController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: GestureDetector(
          onTap: () => FocusScope.of(context).requestFocus(
            FocusNode(),
          ),
          child: Container(
            color: AppColors.primaryColor.shade600,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Positioned(
                  top: 50,
                  width: _screenWidth - 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(
                          Icons.arrow_drop_down_circle_outlined,
                          color: Colors.white,
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.barCodeSearch,
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                        color: TextColor.whiteColor,
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 120,
                  bottom: 0,
                  child: TopCorneredContainerWidget(
                    childWidget: SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          children: [
                            Icon(
                              Icons.qr_code_scanner_sharp,
                              color: Colors.grey.shade800,
                              size: 160,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            ColoredTitleTextWidget(
                              text: AppLocalizations.of(context)!
                                  .enterCompanyCode,
                              fontWeight: FontWeight.w500,
                              fontSize: 16.0,
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            Container(
                              height: 50,
                              padding: const EdgeInsets.fromLTRB(16, 5, 16, 5),
                              decoration: BoxDecoration(
                                color: Colors.grey.shade200,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(30.0),
                                ),
                              ),
                              child: Center(
                                child: TextField(
                                  onChanged: (text) async {},
                                  keyboardType:
                                      const TextInputType.numberWithOptions(),
                                  textInputAction: TextInputAction.search,
                                  cursorColor: AppColors.primaryColor.shade600,
                                  expands: false,
                                  maxLines: 1,
                                  minLines: 1,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    // focusColor: Colors.brown,
                                    // fillColor: Colors.white,
                                    hintStyle: TextStyle(
                                      fontFamily: AppLocalizations.of(context)!
                                          .fontName,
                                    ),
                                    hintText: AppLocalizations.of(context)!
                                        .searchCode,
                                    contentPadding:
                                        const EdgeInsets.fromLTRB(5, 0, 5, 5),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 24,
                            ),
                            Container(
                              height: 50,
                              width: _screenWidth * 0.4,
                              padding: const EdgeInsets.fromLTRB(16, 5, 16, 5),
                              decoration: BoxDecoration(
                                color: AppColors.primaryColor.shade600,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(30.0),
                                ),
                              ),
                              child: Center(
                                child: Row(
                                  children: [
                                    const Icon(
                                      Icons.search,
                                      color: Colors.white,
                                      size: 32,
                                    ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    ColoredTitleTextWidget(
                                      text: AppLocalizations.of(context)!
                                          .search,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16.0,
                                      color: TextColor.whiteColor,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
