import 'package:flutter/material.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Models/profile_option.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileTabPresenter {
  List<ProfileOption> getAllOptions(BuildContext context) {
    List<ProfileOption> options = [];

    options.add(
      ProfileOption(
        icon: Icons.favorite_border,
        title: AppLocalizations.of(context)!.favorites,
        id: 0,
      ),
    );
    options.add(
      ProfileOption(
        icon: Icons.settings_outlined,
        title: AppLocalizations.of(context)!.settings,
        id: 1,
      ),
    );
    options.add(
      ProfileOption(
        icon: Icons.privacy_tip,
        title: AppLocalizations.of(context)!.privacy,
        id: 2,
      ),
    );
    options.add(
      ProfileOption(
        icon: Icons.privacy_tip_rounded,
        title: AppLocalizations.of(context)!.usageConditions,
        id: 3,
      ),
    );
    options.add(
      ProfileOption(
        icon: Icons.contact_phone,
        title: AppLocalizations.of(context)!.contact_us,
        id: 4,
      ),
    );
    options.add(
      ProfileOption(
        icon: Icons.touch_app,
        title: AppLocalizations.of(context)!.about_app,
        id: 5,
      ),
    );

    // options.add(
    //   ProfileOption(
    //     icon: Icons.animation,
    //     title: 'Test',
    //     id: 6,
    //   ),
    // );

    return options;
  }
}
