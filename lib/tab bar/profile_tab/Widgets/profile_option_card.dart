import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/dots_indicator.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/general_webview_widget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Models/profile_option.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/favorite_products/favorite_products_widget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/settings_widgets/settings_widget.dart';

class ProfileOptionCard extends StatefulWidget {
  final ProfileOption option;

  const ProfileOptionCard({Key? key, required this.option}) : super(key: key);

  @override
  _ProfileOptionCardState createState() => _ProfileOptionCardState();
}

class _ProfileOptionCardState extends State<ProfileOptionCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.grey.shade50,
      child: ListTile(
        onTap: () {
          Navigator.of(context).push(
            CupertinoPageRoute(//MaterialPageRoute
              builder: (context) {
                if (widget.option.id == 0) {
                  return const FavoriteProductsWidget();
                } else if (widget.option.id == 1) {
                  return const SettingsWidget();
                } else if (widget.option.id == 6) {
                  // return const AnimateOnScrollFlutter();
                  return const DotsPageWidget();
                } else {
                  return GeneralWebViewWidget(
                    serviceName: widget.option.title,
                  );
                }
              },
            ),
          );
        },
        leading: Icon(
          widget.option.icon,
          color: Colors.green.shade500,
          size: 32,
        ),
        title: ColoredTitleTextWidget(
          text: widget.option.title,
        ),
        trailing: Icon(
          Icons.arrow_forward_ios_outlined,
          color: Colors.grey.shade400,
          size: 20,
        ),
      ),
    );
  }
}
