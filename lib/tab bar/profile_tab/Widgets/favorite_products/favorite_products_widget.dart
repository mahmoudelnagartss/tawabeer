import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/home_tab/models/company_model.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/home_widgets/cards/company_card_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class FavoriteProductsWidget extends StatefulWidget {
  const FavoriteProductsWidget({Key? key}) : super(key: key);

  @override
  _FavoriteProductsWidgetState createState() => _FavoriteProductsWidgetState();
}

class _FavoriteProductsWidgetState extends State<FavoriteProductsWidget> {
  @override
  Widget build(BuildContext context) {
    CompanyModel model = CompanyModel(
      title: AppLocalizations.of(context)!.companyTitle,
      image: 'assets/photo_food_0.jpeg',
      id: '1',
      location: AppLocalizations.of(context)!.companyLocation,
      rate: 4,
      isFavorite: true,
    );

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.favorites,
        height: 60,
      ),
      body: TopCorneredContainerWidget(
        childWidget: StaggeredGridView.countBuilder(
          mainAxisSpacing: 0.0,
          crossAxisSpacing: 1.0,
          crossAxisCount: 8,
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) => CompanyCardWidget(
            height: 0,
            width: 0,
            companyModel: model,
          ),
          staggeredTileBuilder: (int index) => const StaggeredTile.count(4, 5),
        ),
      ),
    );
  }
}
