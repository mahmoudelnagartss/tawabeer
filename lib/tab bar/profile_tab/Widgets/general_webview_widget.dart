
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class GeneralWebViewWidget extends StatefulWidget {
  final String serviceName;

  const GeneralWebViewWidget({
    Key? key,
    required this.serviceName,
  }) : super(key: key);

  @override
  _GeneralWebViewWidgetState createState() => _GeneralWebViewWidgetState();
}

class _GeneralWebViewWidgetState extends State<GeneralWebViewWidget> {
  final _webViewPlugin = FlutterWebviewPlugin();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // on pressing back button, exiting the screen instead of showing loading symbol

    _webViewPlugin.onDestroy.listen((_) {
      if (Navigator.canPop(context)) {
        // exiting the screen
        Navigator.of(context).pop();
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await _webViewPlugin.close();
        return true;
      },
      child: WebviewScaffold(
        url: 'https://www.gosogle.com',
        appBar: AppBarWidget(
          barTitle: widget.serviceName,
        ),
        withZoom: true,
        withLocalStorage: true,
        hidden: true,
        initialChild: Container(
          color: Colors.white,
          child: Center(
            child: SpinKitFadingFour(
              color: AppColors.primaryColor,
              duration: const Duration(
                milliseconds: 700,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
