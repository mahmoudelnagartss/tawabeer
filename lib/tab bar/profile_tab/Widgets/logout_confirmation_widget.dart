import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LogoutConfirmationWidget extends StatelessWidget {
  const LogoutConfirmationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _appProvider = Provider.of<AppProvider>(context, listen: false);
    final Size _screenSize = MediaQuery.of(context).size;

    return TopCorneredContainerWidget(
      height: 250,
      childWidget: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.logout,
                  size: 32,
                  color: Colors.red.shade600,
                ),
                const SizedBox(
                  width: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.logout,
                  fontWeight: FontWeight.bold,
                  color: TextColor.blackColor,
                ),
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.logoutConfirmation,
            ),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
              height: 40,
              width: _screenSize.width,
              child: Center(
                child: Container(
                  height: 40,
                  width: _screenSize.width * 0.6,
                  decoration: BoxDecoration(
                    color: Colors.red.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      _appProvider.setAppHomeWidget(AppHomeWidgets.authWrapper);
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.logout,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
