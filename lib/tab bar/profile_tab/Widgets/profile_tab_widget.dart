import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/login/widgets/EditProfileWidget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/logout_confirmation_widget.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Models/profile_option.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/profile_option_card.dart';
import 'package:tawabeer/tab%20bar/profile_tab/presenter/profile_tab_presenter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileTabWidget extends StatefulWidget {
  const ProfileTabWidget({Key? key}) : super(key: key);

  @override
  _ProfileTabWidgetState createState() => _ProfileTabWidgetState();
}

class _ProfileTabWidgetState extends State<ProfileTabWidget> {
  late List<ProfileOption> _allOptions = [];
  final ProfileTabPresenter _presenter = ProfileTabPresenter();

  @override
  Widget build(BuildContext context) {
    _allOptions = _presenter.getAllOptions(context);

    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        color: Colors.blueGrey[50],
        image: const DecorationImage(
          image: AssetImage(
            'assets/background.png',
          ),
          fit: BoxFit.fill,
        ),
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 50.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  CupertinoPageRoute(
                    maintainState: true,
                    builder: (BuildContext context) {
                      return const EditProfileWidget();
                    },
                  ),
                );
              },
              child: Container(
                width: 120,
                height: 120,
                margin: const EdgeInsets.all(10.0),
                padding: const EdgeInsets.all(6.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: Container(
                  child: Icon(
                    Icons.person_add,
                    color: Colors.grey.shade800,
                    size: 32,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            Container(
              height: 400,
              margin: const EdgeInsets.fromLTRB(16, 0, 16, 0),
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 2,
                    offset: const Offset(0, 3), // changes position of shadow
                  ),
                ],
                borderRadius: const BorderRadius.all(
                  Radius.circular(10.0),
                ),
              ),
              child: ListView.builder(
                itemCount: _allOptions.length,
                padding: const EdgeInsets.only(top: 10.0),
                itemBuilder: (context, index) {
                  return ProfileOptionCard(
                    option: _allOptions[index],
                  );
                },
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
            GestureDetector(
              onTap: () {
                showModalBottomSheet<void>(
                  isScrollControlled: true,
                  context: context,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  // backgroundColor: Colors.transparent,
                  builder: (BuildContext context) {
                    return const LogoutConfirmationWidget();
                  },
                );
              },
              child: Container(
                height: 60,
                margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                  color: Colors.green.shade200,
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.shade300,
                      blurRadius: 2,
                      offset: const Offset(2, 4), // Shadow position
                    ),
                  ],
                ),
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.logout,
                        color: TextColor.appMainColor,
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Icon(
                        Icons.logout,
                        color: Colors.green.shade800,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20.0,
            ),
          ],
        ),
      ),
    );
  }
}
