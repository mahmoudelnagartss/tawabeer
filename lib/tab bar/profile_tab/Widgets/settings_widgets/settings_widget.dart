import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/localization/l10n/l10n.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/settings_widgets/languages_list_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


class SettingsWidget extends StatefulWidget {
  const SettingsWidget({Key? key}) : super(key: key);

  @override
  _SettingsWidgetState createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  bool isToggled = false;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.settings,
        height: 100,
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
        height: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(40.0),
            topRight: Radius.circular(40.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.shade300,
              blurRadius: 2,
              offset: const Offset(2, 4), // Shadow position
            ),
          ],
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.add_alert,
                        size: 32,
                        color: Colors.grey.shade600,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.alerts,
                        fontWeight: FontWeight.bold,
                      ),
                    ],
                  ),
                  FlutterSwitch(
                    height: 24.0,
                    width: 50.0,
                    padding: 4.0,
                    toggleSize: 16.0,
                    borderRadius: 12.0,
                    activeColor: AppColors.primaryColor.shade600,
                    value: isToggled,
                    onToggle: (value) {
                      setState(() {
                        isToggled = value;
                      });
                    },
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.notificationRecommendation,
              ),
              const SizedBox(
                height: 50,
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                height: 1,
                color: Colors.grey.shade300,
              ),
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Icon(
                    Icons.language,
                    size: 32,
                    color: Colors.grey.shade600,
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.appLanguage,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Consumer<LocaleProvider>(
                    builder: (context, localProvider, child) => SizedBox(
                      // width: 20,
                      child: FutureBuilder<Locale>(
                        future: localProvider.getLocaleFromSharedPreferences(),
                        builder: (context, snapshot) {
                          return ColoredTitleTextWidget(
                            text: L10n.getName(
                              snapshot.data ?? L10n.getMainLocal(),
                            ),
                            fontWeight: FontWeight.w500,
                            fontSize: 18.0,
                          );
                        }
                      ),
                    ),
                  ),
                  Container(
                    width: 200,
                    height: 40,
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor.shade100,
                      borderRadius: const BorderRadius.all(
                        Radius.circular(20.0),
                      ),
                    ),
                    child: TextButton(
                      onPressed: () {
                        showModalBottomSheet<void>(
                          context: context,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          // backgroundColor: Colors.transparent,
                          builder: (BuildContext context) {
                            return const LanguageListWidget();
                          },
                        );
                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.changeLanguage,
                        color: TextColor.appMainColor,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
