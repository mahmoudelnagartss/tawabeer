import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/localization/l10n/l10n.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LanguageListWidget extends StatefulWidget {
  const LanguageListWidget({Key? key}) : super(key: key);

  @override
  _LanguageListWidgetState createState() => _LanguageListWidgetState();
}

class _LanguageListWidgetState extends State<LanguageListWidget> {

  @override
  Widget build(BuildContext context) {
    final languageLocalProvider =
        Provider.of<LocaleProvider>(context, listen: false);
    var _allLanguages = L10n.all;

    return TopCorneredContainerWidget(
      height: 300,
      childWidget: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.language,
                size: 32,
                color: Colors.grey.shade600,
              ),
              const SizedBox(
                width: 20,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.chooseLanguage,
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 150,
            child: ListView.builder(
              itemCount: _allLanguages.length,
              padding: const EdgeInsets.only(top: 10.0),
              itemBuilder: (context, index) {
                return Card(
                  color: Colors.grey.shade200,
                  child: ListTile(
                    onTap: () async {
                      await languageLocalProvider.setLocale(_allLanguages[index]);
                      Navigator.of(context, rootNavigator: true).pop();
                    },
                    leading: ColoredTitleTextWidget(
                      text: L10n.getFlag(
                        _allLanguages[index],
                      ),
                      fontSize: 24,
                    ),
                    title: ColoredTitleTextWidget(
                      text: L10n.getName(
                        _allLanguages[index],
                      ),
                    ),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }
}
