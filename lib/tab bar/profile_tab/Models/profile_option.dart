import 'package:flutter/material.dart';

class ProfileOption {
  IconData icon;
  String title;
  int id;

  ProfileOption({ required this.title, required this.icon, required this.id });
}