import 'package:flutter/material.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_calculation_model.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_product_model.dart';
import 'package:tawabeer/tab%20bar/cart_tab/presenter/cart_database_services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
class CartPresenter {
  CartDatabaseServices cartDatabaseServices = CartDatabaseServices.instance;

  Future<List<CartProductModel>> getAllCartProducts(BuildContext context) async {
    List<CartProductModel> list =
        await cartDatabaseServices.getSavedCartProducts();

    if (list.isEmpty) {
      for (int index = 0; index < 1; index++) {
        String title = AppLocalizations.of(context)!.productTitle;
        String image = 'assets/photo_food_$index.jpeg';
        String desc = AppLocalizations.of(context)!. productShortDesc;
        String notes = '';
        String price = AppLocalizations.of(context)!. productPrice;

        CartProductModel cartProductModel = CartProductModel(
          title: title,
          image: image,
          desc: desc,
          notes: notes,
          price: price,
          quantity: 1,
        );

        list.add(cartProductModel);
        await cartDatabaseServices.insertProduct(cartProductModel);
      }

      list = await cartDatabaseServices.getSavedCartProducts();
      debugPrint('List.length ${list.length}');
    }

    return list;
  }

  Future<void> saveUpdatedCart(List<CartProductModel> products) async {
    for (int index = 0; index < products.length; index++) {
      await cartDatabaseServices.updateProduct(products[index]);
    }
  }

  Future<void> saveUpdatedProduct(CartProductModel cartProductModel) async {
    await cartDatabaseServices.updateProduct(cartProductModel);

  }

  CartCalculationModel calculateCartProducts(List<CartProductModel> products) {
    int productsTotalPrice = 0;
    int taxes = 15;

    // for (int index = 0; index < products.length; index++) {
    //   await cartDatabaseServices.updateProduct(products[index]);
    // }

    int totalPrice = taxes + productsTotalPrice;

    return CartCalculationModel(productsTotalPrice: '10 SAR', totalPrice: '25 SAR', taxes: '$taxes SAR');

  }
}
