import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_product_model.dart';

class CartDatabaseServices {
  CartDatabaseServices._privateConstructor();

  static final CartDatabaseServices instance =
      CartDatabaseServices._privateConstructor();

  // only have a single app-wide reference to the database
  static Database? _database;

  Future get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  // this opens the database (and creates it if it doesn't exist)
  static late String path;
  static const _databaseName = "tawabeer_database.db";
  static const _databaseVersion = 1;
  static const _tableCartProductsName = 'CartProduct';

  _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future close() async {
    Database db = await instance.database;
    db.close();
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute(
      'CREATE TABLE $_tableCartProductsName(id INTEGER PRIMARY KEY autoincrement, title TEXT, desc TEXT, image TEXT, price TEXT, notes TEXT, quantity INTEGER)',
    );
    return;
  }

  static Future getFileData() async {
    return getDatabasesPath().then((s) {
      return path = s;
    });
  }

  Future<int> insertProduct(CartProductModel cartProductModel) async {
    Database db = await instance.database;

    var products = await db.query(_tableCartProductsName,
        where: "title = ?", whereArgs: [cartProductModel.title]);

    if (products.isNotEmpty) {
      return -1;
    }

    return await db.insert(
      _tableCartProductsName,
      cartProductModel.convertCartProductModelToMap(),
    );
  }

  Future getSavedCartProductsCount() async {
    var res = await _getSavedCartProductsRawData();
    return res.length;
  }

  Future<List<Map<String, Object?>>> _getSavedCartProductsRawData() async {
    Database db = await instance.database;
    var res =
        await db.query(_tableCartProductsName, orderBy: 'title ASC'); //DESC ASC

    return res;
  }

  Future<List<CartProductModel>> getSavedCartProducts() async {
    var res = await _getSavedCartProductsRawData();

    List<CartProductModel> products = [];
    for (int i = 0; i < res.length; i++) {
      products.add(CartProductModel.fromMap(res[i]));
    }

    return products;
  }

  Future deleteProduct(String title) async {
    Database db = await instance.database;
    var products = db
        .delete(_tableCartProductsName, where: "title = ?", whereArgs: [title]);
    return products;
  }

  Future updateProduct(CartProductModel cartProductModel) async {
    Database db = await instance.database;
    // row to update
    Map<String, dynamic> productMap = cartProductModel.convertCartProductModelToMap();

    int updateCount = await db.update(
        _tableCartProductsName,
        productMap,
        where: 'title = ?',
        whereArgs: [cartProductModel.title]);
  }
}
