import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_product_model.dart';
import 'package:tawabeer/tab%20bar/cart_tab/presenter/cart_presenter.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CartProductCardWidget extends StatefulWidget {
  final CartProductModel cartProductModel;

  const CartProductCardWidget({Key? key, required this.cartProductModel})
      : super(key: key);

  @override
  State<CartProductCardWidget> createState() => _CartProductCardWidgetState();
}

class _CartProductCardWidgetState extends State<CartProductCardWidget> {
  final CartPresenter _cartPresenter = CartPresenter();

  @override
  Widget build(BuildContext context) {
    var txtController = TextEditingController();
    txtController.text = widget.cartProductModel.notes;

    return Container(
      padding: const EdgeInsets.fromLTRB(16, 20, 16, 20),
      height: 230,
      alignment: Alignment.center,
      color: Colors.transparent,
      child: Column(
        children: [
          Container(
            height: 110,
            decoration: BoxDecoration(
              color: Colors.grey.shade100,
              borderRadius: BorderRadius.circular(12),
            ),
            child: Row(
              children: [
                const SizedBox(
                  width: 8,
                ),
                SizedBox(
                  width: 94,
                  height: 94,
                  child: Image.asset(
                    widget.cartProductModel.image,
                    fit: BoxFit.fill,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        ColoredTitleTextWidget(
                          text: widget.cartProductModel.title,
                          fontWeight: FontWeight.w500,
                        ),
                        ColoredTitleTextWidget(
                          text: widget.cartProductModel.desc,
                          fontSize: 13.0,
                        ),

                        SizedBox(
                          height: 40,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ColoredTitleTextWidget(
                                text: widget.cartProductModel.price,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                                color: TextColor.appMainColor,
                              ),
                              const Spacer(),
                              IconButton(
                                icon: Icon(
                                  Icons.add_circle,
                                  color: AppColors.primaryColor.shade800,
                                  size: 24,
                                ),
                                onPressed: () async {
                                  debugPrint('plus');
                                  if (widget.cartProductModel.quantity < 99) {
                                    widget.cartProductModel.quantity++;
                                    await _cartPresenter.saveUpdatedProduct(
                                        widget.cartProductModel);
                                    setState(() {

                                    });
                                  }
                                },
                              ),
                              ColoredTitleTextWidget(
                                text: '${widget.cartProductModel.quantity}',
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.remove_circle_outline,
                                  color: AppColors.primaryColor.shade500,
                                  size: 24,
                                ),
                                onPressed: () async {
                                  debugPrint('remove');
                                  if (widget.cartProductModel.quantity > 0) {
                                    widget.cartProductModel.quantity--;
                                    await _cartPresenter.saveUpdatedProduct(
                                        widget.cartProductModel);
                                    setState(() {});
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            height: 70,
            child: TextField(
              controller: txtController,
              onChanged: (text) async {
                widget.cartProductModel.notes = text;
                await _cartPresenter
                    .saveUpdatedProduct(widget.cartProductModel);
              },
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              cursorColor: AppColors.primaryColor.shade600,
              expands: true,
              maxLines: null,
              minLines: null,
              style: TextStyle(
                fontFamily: AppLocalizations.of(context)!.fontName,
              ),
              decoration: InputDecoration(
                hintStyle: TextStyle(
                  fontFamily: AppLocalizations.of(context)!.fontName,
                ),
                labelStyle: TextStyle(
                  fontFamily: AppLocalizations.of(context)!.fontName,
                ),
                focusColor: Colors.brown,
                filled: true,
                fillColor: Colors.white,
                hintText: AppLocalizations.of(context)!.notes,
                contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.grey.shade100, width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: AppColors.primaryColor.shade300, width: 0.5),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
