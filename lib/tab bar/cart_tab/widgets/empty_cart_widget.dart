import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';

class EmptyCartWidget extends StatelessWidget {
  const EmptyCartWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('EmptyCartWidget');
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
      ),
      width: double.infinity,
      child: Center(
        child: SizedBox(
          height: 300,
          child: Column(
            children: [
              Icon(
                Icons.shopping_cart_outlined,
                color: Colors.grey.shade400,
                size: 100,
              ),
              const SizedBox(
                height: 20,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.emptyCart,
                fontWeight: FontWeight.bold,
              ),
              const SizedBox(
                height: 10,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.refillCart,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
