import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/drop_down_menu_labeled_widget.dart';
import 'package:tawabeer/localization/l10n/l10n.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/order_received_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/cupertino.dart';

class PaymentWidget extends StatefulWidget {
  const PaymentWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<PaymentWidget> createState() => _PaymentWidgetState();
}

class _PaymentWidgetState extends State<PaymentWidget> {
  bool isCashPayment = true;
  String selectedDateString = '';
  DateTime initialDate = DateTime.now();
  DateTime? selectedDate;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.payment,
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(
            FocusNode(),
          );
        },
        child: Container(
          padding: const EdgeInsets.fromLTRB(16, 20, 16, 0),
          width: double.infinity,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: getCardPaymentWidgets(),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> getCardPaymentWidgets() {
    List<Widget> widgets = [];

    widgets.add(
      Row(
        children: [
          Image.asset(
            'assets/paymentSelection.png',
            height: 60,
          ),
          const SizedBox(
            width: 16,
          ),
          ColoredTitleTextWidget(
            text: AppLocalizations.of(context)!.choosePayment,
            fontWeight: FontWeight.bold,
          ),
        ],
      ),
    );
    widgets.add(
      const SizedBox(
        height: 20,
      ),
    );

    widgets.add(
      RadioListTile<bool>(
        value: true,
        groupValue: isCashPayment,
        activeColor: AppColors.primaryColor.shade800,
        title: ColoredTitleTextWidget(
          text: AppLocalizations.of(context)!.payOnReceive,
          fontWeight: FontWeight.bold,
        ),
        onChanged: (value) {
          setState(() {
            isCashPayment = true;
          });
        },
      ),
    );

    widgets.add(
      RadioListTile<bool>(
        value: false,
        groupValue: isCashPayment,
        activeColor: AppColors.primaryColor.shade800,
        title: ColoredTitleTextWidget(
          text: AppLocalizations.of(context)!.payWithATMCards,
          fontWeight: FontWeight.bold,
        ),
        onChanged: (value) {
          setState(() {
            isCashPayment = false;
          });
        },
      ),
    );

    if (!isCashPayment) {
      widgets.add(
        Padding(
          padding: const EdgeInsets.only(left: 30.0, right: 30.0),
          child: SizedBox(
            child: Column(
              children: [
                DropdownMenuLabeledWidget(
                  title: '',
                  list: [
                    AppLocalizations.of(context)!.vISA,
                    AppLocalizations.of(context)!.masterCard,
                  ],
                  width: MediaQuery.of(context).size.width - 100,
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(30.0),
                    ),
                  ),
                  child: TextField(
                    onChanged: (text) async {},
                    keyboardType: const TextInputType.numberWithOptions(),
                    textInputAction: TextInputAction.next,
                    cursorColor: AppColors.primaryColor.shade600,
                    // textAlignVertical: TextAlignVertical.center,
                    expands: false,
                    maxLines: 1,
                    minLines: 1,

                    style: TextStyle(
                      fontFamily: AppLocalizations.of(context)!.fontName,
                    ),
                    decoration: InputDecoration(
                      hintStyle: TextStyle(
                        fontFamily: AppLocalizations.of(context)!.fontName,
                      ),
                      labelStyle: TextStyle(
                        fontFamily: AppLocalizations.of(context)!.fontName,
                      ),
                      hintText: AppLocalizations.of(context)!.cardNumber,
                      focusColor: Colors.brown,
                      filled: true,
                      fillColor: Colors.grey.shade100,
                      contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                  decoration: BoxDecoration(
                    color: Colors.grey.shade100,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(30.0),
                    ),
                  ),
                  child: TextField(
                    onChanged: (text) async {},
                    keyboardType: const TextInputType.numberWithOptions(),
                    textInputAction: TextInputAction.next,
                    cursorColor: AppColors.primaryColor.shade600,
                    // textAlignVertical: TextAlignVertical.center,
                    expands: false,
                    maxLines: 1,
                    minLines: 1,

                    style: TextStyle(
                      fontFamily: AppLocalizations.of(context)!.fontName,
                    ),
                    decoration: InputDecoration(
                      hintStyle: TextStyle(
                        fontFamily: AppLocalizations.of(context)!.fontName,
                      ),
                      labelStyle: TextStyle(
                        fontFamily: AppLocalizations.of(context)!.fontName,
                      ),
                      hintText: AppLocalizations.of(context)!.name,
                      focusColor: Colors.brown,
                      filled: true,
                      fillColor: Colors.grey.shade100,
                      contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                      border: InputBorder.none,
                    ),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Consumer<LocaleProvider>(
                      builder: (context, localProvider, child) => SizedBox(
                        // width: 20,
                        child: FutureBuilder<Locale>(
                            future:
                                localProvider.getLocaleFromSharedPreferences(),
                            builder: (context, snapshot) {
                              return GestureDetector(
                                onTap: () {
                                  showMonthPicker(
                                    context: context,
                                    initialDate: selectedDate ?? initialDate,
                                    firstDate: DateTime(
                                        initialDate.year, initialDate.month),
                                    lastDate:
                                        DateTime(initialDate.year + 10, 12),
                                    locale:
                                        snapshot.data ?? L10n.getMainLocal(),
                                  ).then((date) {
                                    if (date != null) {
                                      selectedDate = date;
                                      var outputFormat =
                                          DateFormat('MM / yyyy');
                                      var outputDate =
                                          outputFormat.format(date);
                                      setState(() {
                                        selectedDateString = outputDate;
                                      });
                                    }
                                  });
                                },
                                child: Container(
                                  height: 50,
                                  width: (MediaQuery.of(context).size.width -
                                          100) /
                                      2,
                                  padding:
                                      const EdgeInsets.fromLTRB(16, 0, 16, 0),
                                  decoration: BoxDecoration(
                                    color: Colors.grey.shade100,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(30.0),
                                    ),
                                  ),
                                  child: Center(
                                    child: ColoredTitleTextWidget(
                                      text: selectedDateString.isEmpty
                                          ? AppLocalizations.of(context)!
                                              .expiryDate
                                          : selectedDateString,
                                      fontSize: 12.0,
                                    ),
                                  ),
                                ),
                              );
                            }),
                      ),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    Container(
                      height: 50,
                      width: (MediaQuery.of(context).size.width - 100) / 2,
                      padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                      decoration: BoxDecoration(
                        color: Colors.grey.shade100,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(30.0),
                        ),
                      ),
                      child: TextField(
                        onChanged: (text) async {},
                        keyboardType: const TextInputType.numberWithOptions(),
                        textInputAction: TextInputAction.done,
                        cursorColor: AppColors.primaryColor.shade600,
                        // textAlignVertical: TextAlignVertical.center,
                        expands: false,
                        maxLines: 1,
                        minLines: 1,
                        style: TextStyle(
                            fontFamily: AppLocalizations.of(context)!.fontName,
                            fontSize: 12.0,
                            letterSpacing: 0.0),
                        decoration: InputDecoration(
                          hintStyle: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                              fontSize: 12.0,
                              letterSpacing: 0.0),
                          labelStyle: TextStyle(
                              fontFamily:
                                  AppLocalizations.of(context)!.fontName,
                              fontSize: 12.0,
                              letterSpacing: 0.0),
                          hintText: AppLocalizations.of(context)!.cVV,
                          focusColor: Colors.brown,
                          filled: true,
                          fillColor: Colors.grey.shade100,
                          contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                          border: InputBorder.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }
    widgets.add(
      SizedBox(
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 40,
              margin: const EdgeInsets.only(top: 30),
              padding: const EdgeInsets.only(right: 20, left: 20),
              decoration: BoxDecoration(
                color: AppColors.primaryColor.shade600,
                borderRadius: BorderRadius.circular(20),
              ),
              child: TextButton(
                onPressed: () {
                  Navigator.of(context).push(
                    CupertinoPageRoute(
                      builder: (BuildContext context) {
                        return OrderReceivedWidget();
                      },
                    ),
                  );
                },
                child: ColoredTitleTextWidget(
                  text: isCashPayment
                      ? AppLocalizations.of(context)!.finishOrder
                      : AppLocalizations.of(context)!.payFinishOrder,
                  fontWeight: FontWeight.bold,
                  color: TextColor.whiteColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );

    return widgets;
  }
}
