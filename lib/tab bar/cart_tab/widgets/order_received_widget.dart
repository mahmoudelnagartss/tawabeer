import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';

class OrderReceivedWidget extends StatelessWidget {
  const OrderReceivedWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primaryColor.shade600,
        title: ColoredTitleTextWidget(
          text: AppLocalizations.of(context)!.receivedOrder,
          fontWeight: FontWeight.bold,
          fontSize: 18.0,
          color: TextColor.whiteColor,
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).popUntil(
              (route) {
                return route.isFirst;
              },
            );
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(8, 20, 8, 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
        ),
        width: double.infinity,
        child: Center(
          child: SizedBox(
            height: 400,
            child: Column(
              children: [
                Image.asset(
                  'assets/receiveOrder.png',
                  height: 150,
                ),
                const SizedBox(
                  height: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.receivedOrder,
                  fontWeight: FontWeight.bold,
                ),
                const SizedBox(
                  height: 10,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.deliveredSoon,
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width * 0.4,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: TextButton(
                      onPressed: () {
                        final _tabBarProvider =
                            Provider.of<TabBarProvider>(context, listen: false);
                        _tabBarProvider.setSelectedTabName(TabNames.orders);
                        Navigator.of(context).popUntil(
                          (route) {
                            return route.isFirst;
                          },
                        );
                      },
                      child: ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.goToOrders,
                        color: TextColor.whiteColor,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
