import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_products_list.dart';

class CartTabWidget extends StatefulWidget {
  const CartTabWidget({
    Key? key,
  }) : super(key: key);

  @override
  _CartTabWidgetState createState() => _CartTabWidgetState();
}

class _CartTabWidgetState extends State<CartTabWidget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        body: NestedScrollView(
          floatHeaderSlivers: false,
          scrollDirection: Axis.vertical,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: SliverSafeArea(
                  top: false,
                  bottom: false,
                  sliver: SliverAppBar(
                    expandedHeight: 120.0,
                    collapsedHeight: 75.0,
                    centerTitle: false,
                    floating: true,
                    pinned: true,
                    snap: false,
                    primary: true,
                    elevation: 0.0,
                    forceElevated: false,
                    title: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.cart,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                      color: TextColor.whiteColor,
                    ),
                    // leading: IconButton(
                    //   icon: const Icon(
                    //     Icons.arrow_drop_down_circle_outlined,
                    //     color: Colors.white,
                    //   ),
                    //   onPressed: () {
                    //     debugPrint('close');
                    //     Navigator.of(context).pop();
                    //   },
                    // ),
                    flexibleSpace: Stack(
                      children: <Widget>[
                        Positioned.fill(
                            child: Container(
                          color: AppColors.primaryColor.shade600,
                        )),
                      ],
                    ),
                  ),
                ),
              ),
              // if you want another component to disappear like 365 tabs
              // SliverPersistentHeader(
              //   delegate: _SliverAppBarDelegate(),
              //   pinned: true,
              // ),
            ];
          },
          body: Container(
            color: AppColors.primaryColor.shade600,
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(
                  FocusNode(),
                );
              },
              child: CartProductsList(),
            ),
          ),
        ),
      ),
    );
  }
}
