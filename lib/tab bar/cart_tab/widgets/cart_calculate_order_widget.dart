import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_calculation_model.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CartCalculateOrderWidget extends StatelessWidget {
  final CartCalculationModel cartCalculationModel;

  const CartCalculateOrderWidget({Key? key, required this.cartCalculationModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(16, 20, 16, 20),
      alignment: Alignment.center,
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.orderTotalProducts,
              ),
              ColoredTitleTextWidget(
                text: cartCalculationModel.productsTotalPrice,
                fontSize: 13.0,
                color: TextColor.blackColor,
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.taxes,
              ),
              ColoredTitleTextWidget(
                text: cartCalculationModel.taxes,
                fontSize: 13.0,
                color: TextColor.blackColor,
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.orderTotalPayment,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: TextColor.blackColor,
              ),
              ColoredTitleTextWidget(
                text: cartCalculationModel.totalPrice,
                fontWeight: FontWeight.bold,
                fontSize: 13.0,
                color: TextColor.appMainColor,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(16, 20, 16, 20),
            height: 1,
            alignment: Alignment.center,
            color: Colors.grey.shade200,
          ),
          const SizedBox(
            height: 20,
          ),
          ColoredTitleTextWidget(
            text: AppLocalizations.of(context)!.addNotes,
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
            color: TextColor.blackColor,
          ),
          const SizedBox(
            height: 5,
          ),
          ColoredTitleTextWidget(
            text: AppLocalizations.of(context)!.addNotesDescription,
            fontSize: 13.0,
            numberOfLines: 2,
          ),
          const SizedBox(
            height: 8,
          ),
          SizedBox(
            height: 100,
            child: TextField(
              onChanged: (text) async {},
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              cursorColor: AppColors.primaryColor.shade600,
              expands: true,
              maxLines: null,
              minLines: null,
              style: TextStyle(
                fontFamily: AppLocalizations.of(context)!.fontName,
              ),
              decoration: InputDecoration(
                hintStyle: TextStyle(
                  fontFamily: AppLocalizations.of(context)!
                      .fontName,
                ),
                labelStyle: TextStyle(
                  fontFamily: AppLocalizations.of(context)!.fontName,
                ),
                focusColor: Colors.brown,
                filled: true,
                fillColor: Colors.white,
                hintText: AppLocalizations.of(context)!.notes,
                contentPadding: const EdgeInsets.fromLTRB(8, 5, 8, 5),
                enabledBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.grey.shade100, width: 2.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: AppColors.primaryColor.shade300, width: 0.5),
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
    );
  }
}
