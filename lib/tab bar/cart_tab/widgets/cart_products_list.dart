import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/cart_tab/models/cart_product_model.dart';
import 'package:tawabeer/tab%20bar/cart_tab/presenter/cart_presenter.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_calculate_order_widget.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/cart_product_card_widget.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/empty_cart_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/cart_tab/widgets/payment_widget.dart';
import 'package:flutter/cupertino.dart';

class CartProductsList extends StatefulWidget {
  const CartProductsList({
    Key? key,
  }) : super(key: key);

  @override
  _CartProductsListState createState() => _CartProductsListState();
}

class _CartProductsListState extends State<CartProductsList> {
  final CartPresenter _cartPresenter = CartPresenter();
  List<CartProductModel> cartProducts = [];

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<CartProductModel>>(
      future: _cartPresenter.getAllCartProducts(context),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          cartProducts = snapshot.data ?? [];

          return cartProducts.isEmpty
              ? const EmptyCartWidget()
              : Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  width: double.infinity,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 16,
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                const SizedBox(
                                  width: 20,
                                ),
                                ColoredTitleTextWidget(
                                  text: AppLocalizations.of(context)!.orders,
                                  fontWeight: FontWeight.bold,
                                ),
                                const SizedBox(
                                  width: 20,
                                ),
                                Container(
                                  height: 32,
                                  padding:
                                      const EdgeInsets.fromLTRB(8, 0, 8, 0),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryColor.shade50,
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  child: Center(
                                    child: ColoredTitleTextWidget(
                                      text: '${cartProducts.length}',
                                      fontWeight: FontWeight.bold,
                                      color: TextColor.appMainColor,
                                    ),
                                  ),
                                ), // number of orders
                              ],
                            ),
                            Container(
                              height: 40,
                              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                              decoration: BoxDecoration(
                                color: AppColors.primaryColor.shade600,
                                borderRadius: BorderRadius.circular(30),
                              ),
                              child: Center(
                                child: TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      CupertinoPageRoute(
                                        builder: (BuildContext context) {
                                          return PaymentWidget();
                                        },
                                      ),
                                    );
                                  },
                                  child: ColoredTitleTextWidget(
                                    text: AppLocalizations.of(context)!
                                        .goToPayment,
                                    fontWeight: FontWeight.bold,
                                    color: TextColor.whiteColor,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Expanded(
                        child: ListView.separated(
                          padding: EdgeInsets.zero,
                          separatorBuilder: (context, index) => const Divider(
                            height: 1,
                            color: Colors.grey,
                          ),
                          itemCount: cartProducts.length + 1,
                          itemBuilder: (context, index) => Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: (index == cartProducts.length)
                                ? CartCalculateOrderWidget(
                                    cartCalculationModel: _cartPresenter
                                        .calculateCartProducts(cartProducts),
                                  )
                                : CartProductCardWidget(
                                    cartProductModel: cartProducts[index],
                                  ),
                          ),
                          scrollDirection: Axis.vertical,
                        ),
                      ),
                    ],
                  ),
                );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
