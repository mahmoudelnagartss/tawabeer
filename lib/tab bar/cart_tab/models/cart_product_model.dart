class CartProductModel {
  String title;
  String desc;
  String image;
  String price;
  String notes;
  int quantity;

  CartProductModel({
    required this.title,
    required this.desc,
    required this.image,
    required this.price,
    required this.notes,
    required this.quantity,
  });

  convertCartProductModelToMap() {
    return {
      'title': title,
      'image': image,
      'desc': desc,
      'notes': notes,
      'price': price,
      'quantity': quantity,
    };
  }

  static fromMap(Map productMap) {
    return CartProductModel(
      title: productMap['title'] ?? '',
      image: productMap['image'] ?? '',
      desc: productMap['desc'] ?? '',
      notes: productMap['notes'] ?? '',
      price: productMap['price'] ?? '',
      quantity: productMap['quantity'] ?? 1,
    );
  }
}
