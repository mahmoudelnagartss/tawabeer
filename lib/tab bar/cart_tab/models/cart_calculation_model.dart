class CartCalculationModel {
  String productsTotalPrice;
  String taxes;
  String totalPrice;

  CartCalculationModel({
    required this.productsTotalPrice,
    required this.totalPrice,
    required this.taxes,
  });
}
