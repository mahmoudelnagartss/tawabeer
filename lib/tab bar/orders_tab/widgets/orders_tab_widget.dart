import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/empty_orders_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/order_card_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrdersTabWidget extends StatefulWidget {
  final BuildContext buildContext;

  const OrdersTabWidget({
    Key? key,
    required this.buildContext,
  }) : super(key: key);

  @override
  _OrdersTabWidgetState createState() => _OrdersTabWidgetState();
}

class _OrdersTabWidgetState extends State<OrdersTabWidget>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  List<Widget> tabs = [];
  List<Widget> tabViews = [];

  int selectedTabIndex = 0;
  List<String> tabsNames = [];
  List<IconData> tabsIcons = [];

  @override
  void initState() {
    super.initState();
    debugPrint('initState _ordersTabWidget');
    tabsNames = [
      AppLocalizations.of(widget.buildContext)!.current,
      AppLocalizations.of(widget.buildContext)!.preparing,
      AppLocalizations.of(widget.buildContext)!.delivered,
    ];
    tabsIcons = [
      Icons.timer,
      Icons.download_outlined,
      Icons.delivery_dining,
    ];

    _tabController = TabController(
      length: tabsNames.length,
      vsync: this,
    );
    _tabController.addListener(
      () {
        setState(
          () {
            selectedTabIndex = _tabController.index;
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    //top tabs names and change for the selected tab
    tabs = [];
    if(_tabController.index != selectedTabIndex) {
      setState(() {
        _tabController.index = selectedTabIndex;
      });
    }

    debugPrint('selectedTabIndex $selectedTabIndex');
    for (int index = 0; index < tabsNames.length; index++) {
      tabs.add(
        Container(
          padding: const EdgeInsets.fromLTRB(16, 8, 16, 8),
          decoration: BoxDecoration(
            color: (index == selectedTabIndex)
                ? Colors.grey.shade800
                : Colors.grey.shade100,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            children: [
              Icon(
                tabsIcons[index],
                color: (index == selectedTabIndex)
                    ? Colors.white
                    : Colors.grey.shade800,
                size: 16,
              ),
              const SizedBox(
                width: 8,
              ),
              ColoredTitleTextWidget(
                text: tabsNames[index],
                fontSize: 12.0,
                color: (index == selectedTabIndex)
                    ? TextColor.whiteColor
                    : TextColor.greyColor,
              ),
            ],
          ),
        ),
      );
    }

    tabViews = [];
    for (int index = 0; index < tabsNames.length; index++) {
      if (index == 1) {
        tabViews.add(
          const EmptyOrdersWidget(),
        );
      } else {
        tabViews.add(
          ListView.builder(
            padding: const EdgeInsets.all(
              0.0,
            ),
            scrollDirection: Axis.vertical,
            itemCount: 3,
            itemBuilder: (_, int index) {
              return const OrderCardWidget();
            },
          ),
        );
      }
    }

    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.orders,
        height: 60,
      ),
      body: TopCorneredContainerWidget(
        childWidget: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TabBar(
              controller: _tabController,
              isScrollable: true,
              indicatorColor: Colors.transparent,
              unselectedLabelColor: Colors.grey,
              labelPadding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              unselectedLabelStyle: const TextStyle(
                fontSize: 16,
                color: Colors.grey,
                fontWeight: FontWeight.w700,
              ),
              labelStyle: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w700,
              ),
              tabs: tabs,
              onTap: (value) {
                setState(() {
                  selectedTabIndex = value;
                });
              },
            ),
            const SizedBox(
              height: 5,
            ),
            Expanded(
              child: TabBarView(
                controller: _tabController,
                children: tabViews,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
