import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';

class EmptyOrdersWidget extends StatelessWidget {
  const EmptyOrdersWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _tabBarProvider = Provider.of<TabBarProvider>(context, listen: false);

    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
      ),
      width: double.infinity,
      child: Center(
        child: SizedBox(
          height: 350,
          child: Column(
            children: [
              Image.asset(
                'assets/ordersEmpty.png',
                height: 150,
                width: 150,
              ),
              const SizedBox(
                height: 20,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.products,
                fontWeight: FontWeight.w600,
                fontSize: 14.0,
                color: TextColor.blackColor,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.noOrders,
              ),
              const SizedBox(
                height: 10,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.noOrdersDesc,
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                  color: AppColors.primaryColor.shade600,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Center(
                  child: TextButton(
                    onPressed: () {
                      _tabBarProvider.setSelectedTabName(TabNames.home);
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.goToOrder,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
