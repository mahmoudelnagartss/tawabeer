import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderDetailsHeaderWidget extends StatelessWidget {
  const OrderDetailsHeaderWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.orderDetails,
              fontWeight: FontWeight.w600,
              fontSize: 14.0,
              color: TextColor.blackColor,
            ),
            Container(
              width: _screenWidth * .4,
              padding: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.grey.shade100,
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Center(
                child: ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.receivedOrder,
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.orderNumber,
            ),
            const ColoredTitleTextWidget(
              text: '20141232',
              fontWeight: FontWeight.w600,
            ),
          ],
        ),
        const SizedBox(
          height: 4,
        ),
        const ColoredTitleTextWidget(
          text: '22-10-2021 10:30 AM',
          fontSize: 12.0,
        ),
        const SizedBox(
          height: 16,
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: Colors.grey.shade200,
        ),
        const SizedBox(
          height: 16,
        ),
        ColoredTitleTextWidget(
          text: AppLocalizations.of(context)!.restaurantDetails,
          fontWeight: FontWeight.w600,
          fontSize: 14.0,
          color: TextColor.blackColor,
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            AvatarView(
              radius: 32,
              borderColor: AppColors.primaryColor,
              borderWidth: 0,
              isOnlyText: false,
              avatarType: AvatarType.CIRCLE,
              backgroundColor: AppColors.primaryColor.shade600,
              imagePath: "assets/photo_food_0.jpeg",
              placeHolder: const Icon(
                Icons.home_filled,
              ),
              errorWidget: const Icon(
                Icons.home_filled,
              ),
            ),
            const SizedBox(
              width: 8,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.companyTitle,
              fontWeight: FontWeight.w600,
              fontSize: 14.0,
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Icon(
                  Icons.location_on,
                  size: 24,
                  color: Colors.grey.shade600,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.companyLocation,
                  fontWeight: FontWeight.w600,
                  fontSize: 14.0,
                ),
              ],
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                padding: const EdgeInsets.fromLTRB(8, 2, 8, 2),
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.grey,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.map_sharp,
                      color: Colors.grey.shade600,
                      size: 24,
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.openOnMaps,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 16,
        ),
        Container(
          width: double.infinity,
          height: 1,
          color: Colors.grey.shade200,
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
