import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/cancel_order_widget.dart';

class OrderPaymentStatusWidget extends StatefulWidget {
  const OrderPaymentStatusWidget({Key? key}) : super(key: key);

  @override
  _OrderPaymentStatusWidgetState createState() =>
      _OrderPaymentStatusWidgetState();
}

class _OrderPaymentStatusWidgetState extends State<OrderPaymentStatusWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ColoredTitleTextWidget(
          text: AppLocalizations.of(context)!.payment,
          fontWeight: FontWeight.w600,
          fontSize: 14.0,
          color: TextColor.blackColor,
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 3,
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                ),
                child: Center(
                  child: ColoredTitleTextWidget(
                    text: AppLocalizations.of(context)!.vISA,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                    color: TextColor.blackColor,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 2,
            ),
            Expanded(
              flex: 7,
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                  color: Colors.grey.shade200,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                ),
                child: const Center(
                  child: ColoredTitleTextWidget(
                    text: '**** **** **** 6572',
                    fontWeight: FontWeight.w600,
                    fontSize: 14.0,
                    color: TextColor.blackColor,
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        GestureDetector(
          onTap: () {
            showModalBottomSheet<void>(
              context: context,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              // backgroundColor: Colors.transparent,
              builder: (BuildContext context) {
                return const CancelOrderWidget();
              },
            );
          },
          child: Container(
            height: 40,
            decoration: BoxDecoration(
              color: Colors.red.shade600,
              borderRadius: const BorderRadius.all(
                Radius.circular(20.0),
              ),
            ),
            child: Center(
              child: ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.cancelOrder,
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
                color: TextColor.whiteColor,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 50,
        ),
      ],
    );
  }
}
