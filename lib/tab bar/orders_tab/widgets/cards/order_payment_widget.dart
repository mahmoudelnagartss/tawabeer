import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class OrderPaymentWidget extends StatelessWidget {
  const OrderPaymentWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.orderTotalProducts,
              fontWeight: FontWeight.w600,
            ),

            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.productsPrice,
              fontSize: 13.0,
              color: TextColor.blackColor,
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.taxes,
              fontWeight: FontWeight.w600,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.productsPrice,
              fontSize: 13.0,
              color: TextColor.blackColor,
            ),
          ],
        ),
        const SizedBox(
          height: 8,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.orderTotalPayment,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
              color: TextColor.blackColor,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.productsPrice,
              fontWeight: FontWeight.bold,
              fontSize: 13.0,
              color: TextColor.appMainColor,
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(16, 20, 16, 20),
          height: 1,
          alignment: Alignment.center,
          color: Colors.grey.shade200,
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
