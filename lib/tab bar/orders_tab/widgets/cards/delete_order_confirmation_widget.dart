import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DeleteOrderConfirmationWidget extends StatelessWidget {
  const DeleteOrderConfirmationWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Size _screenSize = MediaQuery.of(context).size;
    return TopCorneredContainerWidget(
      height: 360,
      childWidget: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                  Icons.delete,
                  size: 32,
                  color: Colors.red.shade600,
                ),
                const SizedBox(
                  width: 20,
                ),
                ColoredTitleTextWidget(
                  text: AppLocalizations.of(context)!.deleteOrder,
                  fontWeight: FontWeight.bold,
                  color: TextColor.blackColor,
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.deleteOrderConfirmation,
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              margin: const EdgeInsets.all(8.0),
              decoration: BoxDecoration(
                color: Colors.grey.shade200,
                borderRadius: const BorderRadius.all(
                  Radius.circular(8.0),
                ),
              ),
              child: Row(
                children: [
                  AvatarView(
                    radius: 40,
                    borderColor: Colors.grey.shade400,
                    borderWidth: 2,
                    isOnlyText: false,
                    avatarType: AvatarType.CIRCLE,
                    backgroundColor: AppColors.primaryColor.shade600,
                    imagePath: 'assets/photo_food_0.jpeg',
                    placeHolder: const Icon(
                      Icons.e_mobiledata_sharp,
                    ),
                    errorWidget: const Icon(
                      Icons.e_mobiledata_sharp,
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Flexible(
                    child: Card(
                      color: Colors.grey.shade200,
                      elevation: 0.0,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ColoredTitleTextWidget(
                            text: AppLocalizations.of(context)!.companyTitle,
                            fontWeight: FontWeight.w600,
                            fontSize: 14.0,
                            color: TextColor.blackColor,
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          const ColoredTitleTextWidget(
                            text: '22-10-2021 10:30 AM',
                            fontSize: 12.0,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                child: ColoredTitleTextWidget(
                                  text: AppLocalizations.of(context)!.orderNumber,
                                ),
                              ),
                              const Expanded(
                                // flex: 1,
                                child: ColoredTitleTextWidget(
                                  text: '20141232',
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14.0,
                                  color: TextColor.appMainColor,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 40,
              width: _screenSize.width,
              child: Center(
                child: Container(
                  height: 40,
                  width: _screenSize.width * 0.4,
                  decoration: BoxDecoration(
                    color: Colors.red.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.deleteOrder,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
