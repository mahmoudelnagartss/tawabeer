import 'package:avatar_view/avatar_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/delete_order_confirmation_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/order_details_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderCardWidget extends StatelessWidget {
  const OrderCardWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          CupertinoPageRoute(
            builder: (BuildContext context) {
              return const OrderDetailsWidget();
            },
            // fullscreenDialog: true,
            //push from bottom with x as back icon
          ),
        );

        // Navigator.of(context).push(
        //   PageTransition(
        //     child: const ProductDetailsWidget(),
        //     transitionType: TransitionTypes.slideFromRight,
        //   ),
        // );
      },
      child: Container(
        padding: const EdgeInsets.all(8.0),
        margin: const EdgeInsets.all(8.0),
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          ),
        ),
        child: Slidable(
          direction: Axis.horizontal,
          key: UniqueKey(),
          startActionPane: ActionPane(
            // A motion is a widget used to control how the pane animates.
            motion: const DrawerMotion(),
            dragDismissible: false,
            // A pane can dismiss the Slidable.
            dismissible: DismissiblePane(
              onDismissed: () {},
            ),
            // All actions are defined in the children parameter.
            children: [
              // A Slidable Action can have an icon and/or a label.
              SlidableAction(
                onPressed: (context) {
                  showModalBottomSheet<void>(
                    context: context,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    // backgroundColor: Colors.transparent,
                    builder: (BuildContext context) {
                      return const DeleteOrderConfirmationWidget();
                    },
                  );
                },
                backgroundColor: const Color(0xFFFE4A49),
                foregroundColor: Colors.white,
                icon: Icons.delete_rounded,
                label: AppLocalizations.of(context)!.delete,
              ),
            ],
          ),
          child: Row(
            children: [
              AvatarView(
                radius: 40,
                borderColor: Colors.grey.shade400,
                borderWidth: 2,
                isOnlyText: false,
                avatarType: AvatarType.CIRCLE,
                backgroundColor: AppColors.primaryColor.shade600,
                imagePath: 'assets/photo_food_0.jpeg',
                placeHolder: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
                errorWidget: const Icon(
                  Icons.e_mobiledata_sharp,
                ),
              ),
              const SizedBox(
                width: 8,
              ),
              Flexible(
                child: Card(
                  color: Colors.grey.shade200,
                  elevation: 0.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ColoredTitleTextWidget(
                        text: AppLocalizations.of(context)!.companyTitle,
                        fontWeight: FontWeight.w600,
                        fontSize: 14.0,
                        color: TextColor.blackColor,
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const ColoredTitleTextWidget(
                        text: '22-10-2021 10:30 AM',
                        fontSize: 12.0,
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ColoredTitleTextWidget(
                            text: AppLocalizations.of(context)!.orderNumber,
                          ),
                          // const SizedBox(
                          //   width: 4,
                          // ),
                          Text(
                            '20141232',
                            style: TextStyle(
                              color: AppColors.primaryColor.shade700,
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
