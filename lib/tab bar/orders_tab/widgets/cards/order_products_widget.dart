import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/tab%20bar/home_tab/widgets/company_details_widgets/cards/product_card_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderProductsWidget extends StatefulWidget {
  final BuildContext buildContext;

  const OrderProductsWidget({
    Key? key,
    required this.buildContext,
  }) : super(key: key);

  @override
  State<OrderProductsWidget> createState() => _OrderProductsWidgetState();
}

class _OrderProductsWidgetState extends State<OrderProductsWidget> {
  List<Widget> productsWidgets = [];

  @override
  void initState() {
    super.initState();

    productsWidgets.add(
      Text(
        AppLocalizations.of(widget.buildContext)!.products,
        style: TextStyle(
          color: Colors.black,
          fontSize: 14,
          fontWeight: FontWeight.w600,
          fontFamily: AppLocalizations.of(widget.buildContext)!.fontName,
        ),
      ),
    );

    for (int index = 0; index < 3; index++) {
      productsWidgets.add(
        const ProductCardWidget(shouldShowCartIcon: false),
      );
    }

    productsWidgets.add(
      const SizedBox(
        height: 16,
      ),
    );
    productsWidgets.add(
      Container(
        width: double.infinity,
        height: 1,
        color: Colors.grey.shade200,
      ),
    );
    productsWidgets.add(
      const SizedBox(
        height: 16,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double _screenWidth = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: productsWidgets,
    );
  }
}
