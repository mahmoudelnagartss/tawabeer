import 'package:flutter/material.dart';
import 'package:tawabeer/app%20files/app_bar_widget.dart';
import 'package:tawabeer/general_widgets/top_cornered_container_widget.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/order_details_header_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/order_payment_status_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/order_payment_widget.dart';
import 'package:tawabeer/tab%20bar/orders_tab/widgets/cards/order_products_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OrderDetailsWidget extends StatefulWidget {
  const OrderDetailsWidget({Key? key}) : super(key: key);

  @override
  _OrderDetailsWidgetState createState() => _OrderDetailsWidgetState();
}

class _OrderDetailsWidgetState extends State<OrderDetailsWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      appBar: AppBarWidget(
        barTitle: AppLocalizations.of(context)!.orderDetails,
        height: 60,
      ),
      body: TopCorneredContainerWidget(
        childWidget: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:  [
                const OrderDetailsHeaderWidget(),
                OrderProductsWidget(buildContext: context),
                const OrderPaymentWidget(),
                const OrderPaymentStatusWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
