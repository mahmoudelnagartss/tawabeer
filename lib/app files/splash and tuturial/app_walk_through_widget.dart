import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';
import 'package:tawabeer/app%20files/splash%20and%20tuturial/walk_through_card_widget.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/login/widgets/authentication_wrapper.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/tab%20bar/profile_tab/Widgets/settings_widgets/languages_list_widget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppWalkThroughWidget extends StatefulWidget {
  const AppWalkThroughWidget({Key? key}) : super(key: key);

  static const _kDuration = Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  @override
  State<AppWalkThroughWidget> createState() => _AppWalkThroughWidgetState();
}

class _AppWalkThroughWidgetState extends State<AppWalkThroughWidget> {
  final _controller = PageController();

  final List _samplePages = [
    const WalkThroughCardWidget(index: 1),
    const WalkThroughCardWidget(index: 2),
    const WalkThroughCardWidget(index: 3),
    const WalkThroughCardWidget(index: 4),
  ];

  @override
  void initState() {
    super.initState();

    Future.delayed(
      const Duration(
        seconds: 2,
      ),
      () => showModalBottomSheet<void>(
        context: context,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        builder: (BuildContext context) {
          return const LanguageListWidget();
        },
      ),
      // ---- End of Fire Store Demo -------------
    );
  }

  @override
  Widget build(BuildContext context) {
    final _appProvider = Provider.of<AppProvider>(context, listen: false);

    return Scaffold(
      body: Column(
        children: <Widget>[
          Flexible(
            child: PageView.builder(
              controller: _controller,
              itemCount: _samplePages.length,
              itemBuilder: (BuildContext context, int index) {
                return _samplePages[index % _samplePages.length];
              },
            ),
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: TextButton(
                    onPressed: () {
                      _controller.previousPage(
                          duration: AppWalkThroughWidget._kDuration,
                          curve: AppWalkThroughWidget._kCurve);
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.previous,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    color: AppColors.primaryColor.shade600,
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: TextButton(
                    onPressed: () {
                      _controller.nextPage(
                          duration: AppWalkThroughWidget._kDuration,
                          curve: AppWalkThroughWidget._kCurve);
                    },
                    child: ColoredTitleTextWidget(
                      text: AppLocalizations.of(context)!.next,
                      fontWeight: FontWeight.bold,
                      color: TextColor.whiteColor,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: () {
              debugPrint('setAppHomeWidget');
              _appProvider.setAppHomeWidget(
                AppHomeWidgets.authWrapper,
              );

              // Navigator.of(context).pushAndRemoveUntil(
              //   CupertinoPageRoute(
              //     // maintainState: true,
              //     builder: (BuildContext context) {
              //       return const AuthenticationWrapperWidget();
              //     },
              //   ),
              //   (route) => false,
              // );
            },
            child: ColoredTitleTextWidget(
              text: AppLocalizations.of(context)!.skip,
              fontWeight: FontWeight.bold,
              color: TextColor.appMainColor,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
