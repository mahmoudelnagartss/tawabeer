import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';

class WalkThroughCardWidget extends StatelessWidget {
  final int index;

  const WalkThroughCardWidget({
    Key? key,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Padding(
      padding: const EdgeInsets.fromLTRB(50, 50, 50, 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'assets/tutorial.png',fit: BoxFit.fitWidth,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,

            children: [
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.tutorialTitle,
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: TextColor.blackColor,
                numberOfLines: 2,
              ),
              const SizedBox(
                height: 16.0,
              ),
              ColoredTitleTextWidget(
                text: AppLocalizations.of(context)!.tutorialDescription,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
