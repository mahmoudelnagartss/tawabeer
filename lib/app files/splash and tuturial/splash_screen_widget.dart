import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:tawabeer/app%20files/AppHomeWidgetWrapper.dart';
import 'package:tawabeer/services/app_colors.dart';
import 'package:tawabeer/services/page_transition.dart';

class SplashScreenWidget extends StatefulWidget {
  const SplashScreenWidget({Key? key}) : super(key: key);

  @override
  State<SplashScreenWidget> createState() => _SplashScreenWidgetState();
}

class _SplashScreenWidgetState extends State<SplashScreenWidget>
    with SingleTickerProviderStateMixin {
  // late final AnimationController _controller = AnimationController(
  //   duration: const Duration(seconds: 2),
  //   vsync: this,
  // )..repeat(reverse: true);
  //
  // late final Animation<double> _animation = CurvedAnimation(
  //   parent: _controller,
  //   curve: Curves.easeInOutCirc,
  // );

  bool playGiff = true;

  @override
  void initState() {
    super.initState();
    Future.delayed(
      const Duration(
        seconds: 3,
      ),
      () {
        setState(() {
          playGiff = false;
        });
        return Future.delayed(
          const Duration(
            seconds: 2,
          ),
          () => Navigator.of(context).pushReplacement(
            PageTransition(
              // child: const MainTabBarWidget(),
              child: const AppHomeWidgetWrapper(),
              transitionType: TransitionTypes.fade,
              duration: const Duration(
                milliseconds: 400,
              ),
            ),
          ),
          // ---- End of Fire Store Demo -------------
        );
      },
    );
  }

  // @override
  // void dispose() {
  //   _controller.dispose();
  //   super.dispose();
  // }
  @override
  Widget build(BuildContext context) {
    bool lightMode =
        MediaQuery.of(context).platformBrightness == Brightness.light;
    return Scaffold(
      backgroundColor: AppColors.primaryColor.shade600,
      body: playGiff
          ? Center(
              child: Image.network(
                'https://media.giphy.com/media/TuNxyC7lcikYzvBcYl/giphy.gif',
                gaplessPlayback: false,
                fit: BoxFit.fill,
              ),
            )
          : Container(
              decoration: BoxDecoration(
                color: AppColors.primaryColor.shade800,
                image: const DecorationImage(
                  alignment: Alignment.topCenter,
                  fit: BoxFit.fitWidth,
                  image: AssetImage(
                    'assets/bgProduct.png',
                  ),
                ),
              ),
              child: const Center(
                child: SpinKitThreeBounce(
                  color: Colors.white,
                  duration: Duration(
                    seconds: 1,
                  ),
                ),
              ),
            ),
    );
  }
}
