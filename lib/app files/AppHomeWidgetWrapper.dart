
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';

class AppHomeWidgetWrapper extends StatelessWidget {
  const AppHomeWidgetWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppProvider>(
      builder: (context, appProvider, child) {
        return FutureBuilder<Widget>(
          future: appProvider.getAppHomeWidget(context),
          builder: (context, snapshot) {
            return snapshot.data ?? const SizedBox.shrink();
          },
        );
      },
    );
  }
}
