import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/splash%20and%20tuturial/app_walk_through_widget.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';
import 'package:tawabeer/login/widgets/authentication_wrapper.dart';
import 'package:tawabeer/tab%20bar/TabBar/TabBarWrapper.dart';
import 'package:tawabeer/tab%20bar/TabBar/Provider/TabBarProvider.dart';

enum AppHomeWidgets {
  walkThrough,
  tabBar,
  authWrapper,
}

class AppProvider extends ChangeNotifier {
  Widget? _homeWidget;
  int _homeWidgetIndex = 0;

  setAppHomeWidget(AppHomeWidgets widget) {
    switch (widget) {
      case AppHomeWidgets.tabBar:
        _homeWidgetIndex = 2;
        _homeWidget = ChangeNotifierProvider<TabBarProvider>(
          create: (context) => TabBarProvider(),
          builder: (context, child) => const TabBarWrapper(),
        );
        break;
      case AppHomeWidgets.authWrapper:
        _homeWidgetIndex = 1;
        _homeWidget = const AuthenticationWrapperWidget();
        break;
      default:
        _homeWidgetIndex = 0;
        _homeWidget = const AppWalkThroughWidget();
    }

    notifyListeners();
  }

  Future<Widget> getAppHomeWidget(BuildContext context) async {
    if (_homeWidget == null) {
      final _localProvider =
          Provider.of<LocaleProvider>(context, listen: false);
      bool isLocalSet = await _localProvider.checkLocalSavedLocale();

      if (isLocalSet) {
        setAppHomeWidget(AppHomeWidgets.tabBar);
      } else {
        setAppHomeWidget(AppHomeWidgets.walkThrough);
      }
    }

    debugPrint('$_homeWidget');
    return _homeWidget!;
  }

  String getHomeIndex() {
    debugPrint('key$_homeWidgetIndex');
    return 'key$_homeWidgetIndex';
  }
}
