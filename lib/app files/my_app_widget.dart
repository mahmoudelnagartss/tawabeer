import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/splash%20and%20tuturial/splash_screen_widget.dart';
import 'package:tawabeer/localization/l10n/l10n.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:tawabeer/login/widgets/authentication_wrapper.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final _localProvider = Provider.of<LocaleProvider>(context);

    return OverlaySupport.global(
      child: FutureBuilder<Locale>(
        future: _localProvider.getLocaleFromSharedPreferences(),
        builder: (context, snapshot) {
          return MaterialApp(
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: L10n.all,
            locale: snapshot.data ?? L10n.getMainLocal(),
            // theme: ThemeData(
            //   fontFamily: 'DroidKufi',
            // ),
            routes: {
              '/': (context) => const SplashScreenWidget(),
              '/AuthenticationWrapperWidget': (context) => const AuthenticationWrapperWidget(),
            },
          );
        },
      ),
    );
  }
}
