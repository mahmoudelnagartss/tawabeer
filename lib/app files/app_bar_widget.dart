import 'package:flutter/material.dart';
import 'package:tawabeer/general_widgets/Text/colored_title_text_widget.dart';
import 'package:tawabeer/services/app_colors.dart';

class AppBarWidget extends StatelessWidget with PreferredSizeWidget {
  final String barTitle;
  final Color? bgColor;
  final double? height;
  final List<Widget>? actions;

  const AppBarWidget(
      {Key? key,
      required this.barTitle,
      this.bgColor,
      this.height,
      this.actions})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: ColoredTitleTextWidget(
        text: barTitle,
        fontWeight: FontWeight.bold,
        fontSize: 16.0,
        color: TextColor.whiteColor,
      ),
      backgroundColor: bgColor ?? AppColors.primaryColor.shade600,
      actions: actions,
      elevation: 0.0,
      centerTitle: false,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height ?? kToolbarHeight);
}
