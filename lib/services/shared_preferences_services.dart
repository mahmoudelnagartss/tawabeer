import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesServices {

  Future<String> getSavedValue(String key) async {
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    String _savedValue = _pref.getString(key) ?? '';

    return _savedValue;
  }

  Future<bool> setValueForKey(String key, String value) async {
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    final String _savedPassword = _pref.getString(key) ?? '';

    return _pref.setString(key, value);
  }

  Future<bool> removeValueForKey(String key) async {
    final SharedPreferences _pref = await SharedPreferences.getInstance();
    return _pref.remove(key);
  }
}
