// import 'package:flutter/material.dart';
// import 'package:hs_demo/Model/news_model.dart';
// import 'dart:convert';
// import 'package:http/http.dart';
//
// class NewsAPIService {
//   Future<List<NewsModel>> getNewsList() async {
//     try {
//       Map<String, String> headers = {};
//       // header["content-type"] =  "application/x-www-form-urlencoded";
//       headers["x-uqu-auth"] = "b45f76c9fe68653c290ec8049fed5c5e";
//
//       var uri = Uri.https('uqu.edu.sa', '/en/Api/v1/Cms/NewsCats/');
//
//       debugPrint(' get NewsCats: $uri');
//       debugPrint(' uri $uri');
//
//       Response response = await get(uri, headers: headers);
//       Map result = jsonDecode(response.body);
//
//       String status = result['status'];
//       if (status == 'success') {
//         Map parentData = result['data'];
//         Map newsJson = parentData['news'];
//         List<dynamic> dataList = newsJson['data'];
//         debugPrint('newsJson $dataList');
//         List<NewsModel> allModels =
//             dataList.map((item) => NewsModel(item)).toList();
//
//         return allModels;
//       } else {
//         return [];
//       }
//
//       // String utcOffset = data['utc_offset'].substring(1, 3);
//       // DateTime dateTime = DateTime.parse(datetime);
//       // dateTime = dateTime.add(Duration(hours: int.parse(utcOffset)));
//       //
//       // isDayTime = (dateTime.hour > 6 && dateTime.hour < 20) ? true : false;
//       // bgImageName = isDayTime ? 'assets/day.png' : 'assets/night.png';
//       //
//       // time = DateFormat().format(dateTime);
//     } catch (error) {
//       debugPrint('caught error $error');
//       return [];
//     }
//   }
// }
