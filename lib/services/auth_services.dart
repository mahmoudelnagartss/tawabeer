// import 'dart:async';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/widgets.dart';
// import 'package:hs_demo/Model/user_model.dart';
// import 'package:hs_demo/services/database_services.dart';
//
// class AuthServices {
//   final FirebaseAuth _auth = FirebaseAuth.instance;
//
// //list to firebase auth stream
//
//   // Stream<UserModel> get user {
//   //   return _auth.authStateChanges().map((User? user) => UserModel(user));
//   // }
//
// // this is a stream listner
// // auth change user stream it notify any provider
//   Stream<UserModel> get user {
//     return _auth.authStateChanges().map((user) {
//       return UserModel(user);
//     });
//   }
//
//   // sign anonymous user
//   Future<UserModel?> signInAnonimous() async {
//     try {
//       UserCredential result = await _auth.signInAnonymously();
//       UserModel user = UserModel(result.user);
//       if (user.uid.isNotEmpty) {
//         DatabaseServices _databaseServices = DatabaseServices(userId: user.uid);
//         await _databaseServices.updateUserData('0', 'New Brew', 100);
//         debugPrint('updateUserData $user.uid');
//       }
//       return user;
//     } catch (error) {
//       debugPrint(error.toString());
//
//       return null;
//     }
//   }
//
//   // sign with email and password
//   Future<UserModel?> signInWith(String email, String password) async {
//     try {
//       UserCredential result = await _auth.signInWithEmailAndPassword(
//           email: email, password: password);
//       UserModel user = UserModel(result.user);
//
//       if (user.uid.isNotEmpty) {
//         debugPrint('updateUserData $user.uid');
//       }
//
//       return user;
//     } catch (error) {
//       debugPrint(error.toString());
//
//       return null;
//     }
//   }
//   //register user
//
//   Future<UserModel?> registerWith(String email, String password) async {
//     try {
//       UserCredential result = await _auth.createUserWithEmailAndPassword(
//           email: email, password: password);
//       UserModel user = UserModel(result.user);
//
//       if (user.uid.isNotEmpty) {
//         DatabaseServices _databaseServices = DatabaseServices(userId: user.uid);
//         await _databaseServices.updateUserData('0', 'New Crew', 100);
//         debugPrint('updateUserData $user.uid');
//       }
//
//       return user;
//     } catch (error) {
//       debugPrint(error.toString());
//
//       return null;
//     }
//   }
//
//   //signout
//
//   Future signOut() async {
//     try {
//       return await _auth.signOut();
//     } catch (error) {
//       debugPrint('AuthServices signout error: $error.toString()');
//       return;
//     }
//   }
// }
