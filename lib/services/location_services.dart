import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:geolocator/geolocator.dart';
import 'package:tawabeer/services/app_permissions_services.dart';

class LocationServices {
  Future<bool> _isLocationServiceEnabled() async {
    bool serviceEnabled;
    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (serviceEnabled) {
      debugPrint('Location services are enabled.');
      return true;
    } else {
      debugPrint('Location services are disabled.');
      return false;
    }
  }

  Future<bool> _isLocationPermissionPermitted() async {
    bool serviceEnabled = await _isLocationServiceEnabled();

    if (serviceEnabled) {
      LocationPermission permission = await Geolocator.checkPermission();
      debugPrint('Location services .${permission.name}');

      if (_checkPermissions(permission)) {
        return true;
      } else {
        permission = await Geolocator.requestPermission();

        if (_checkPermissions(permission)) {
          return false;
        } else {
          return true;
        }
      }
    }

    return false;
  }

  bool _checkPermissions(LocationPermission permission) {
    if (permission == LocationPermission.denied ||
        permission == LocationPermission.deniedForever) {
      debugPrint('Location permissions are not permitted');
      return false;
    }

    return true;
  }

  Future<LatLng> getCurrentLocation() async {
    debugPrint('Into getCurrentLocation');
    bool isPermissionGranted = await AppPermissionsServices().requestLocationPermission();

    if (isPermissionGranted) {
      debugPrint('Location permissions granted');
      // continue accessing the position of the device.
      Position _position = await Geolocator.getCurrentPosition();
      debugPrint('$_position');

      return LatLng(_position.latitude, _position.longitude);
    }
    // return default location
    debugPrint('Location permissions not granted');
    return getDefaultLocation();
  }

  LatLng getDefaultLocation() {
    LatLng _defaultTarget = const LatLng(22.69316059975726, 44.732566999254615);

    return _defaultTarget;
  }

// using Location Third party
//   Future<LatLng?> _getCurrentLocationWithLocationPackage() async {
//     Location location = Location();
//
//     bool _serviceEnabled;
//     PermissionStatus _permissionGranted;
//     LocationData _locationData;
//
//     _serviceEnabled = await location.serviceEnabled();
//     if (!_serviceEnabled) {
//       _serviceEnabled = await location.requestService();
//       if (!_serviceEnabled) {
//         return null;
//       }
//     }
//
//     _permissionGranted = await location.hasPermission();
//     if (_permissionGranted == PermissionStatus.denied) {
//       _permissionGranted = await location.requestPermission();
//       if (_permissionGranted != PermissionStatus.granted) {
//         return null;
//       }
//     }
//
//     _locationData = await location.getLocation();
//
//     return LatLng(
//         _locationData.latitude ?? 0.0, _locationData.longitude ?? 0.0);
//   }
}
