// import 'dart:io';
// import 'package:hs_demo/Model/product_model.dart';
// import 'package:path/path.dart';
// import 'package:sqflite/sqflite.dart';
// import 'package:path_provider/path_provider.dart';
//
// class LocalDatabaseServices {
//   LocalDatabaseServices._privateConstructor();
//
//   static final LocalDatabaseServices instance =
//       LocalDatabaseServices._privateConstructor();
//
//   // only have a single app-wide reference to the database
//   static Database? _database;
//
//   Future get database async {
//     if (_database != null) return _database;
//     // lazily instantiate the db the first time it is accessed
//     _database = await _initDatabase();
//     return _database;
//   }
//
//   // this opens the database (and creates it if it doesn't exist)
//   static late String path;
//   static const _databaseName = "hsdatabase.db";
//   static const _databaseVersion = 1;
//
//   _initDatabase() async {
//     Directory documentsDirectory = await getApplicationDocumentsDirectory();
//     String path = join(documentsDirectory.path, _databaseName);
//     return await openDatabase(path,
//         version: _databaseVersion, onCreate: _onCreate);
//   }
//
//   Future close() async {
//     Database db = await instance.database;
//     db.close();
//   }
//
//   static const _tableProduct = 'products';
//
//   // SQL code to create the database table
//   Future _onCreate(Database db, int version) async {
//     await db.execute(
//       // "CREATE TABLE $_tableProduct(name TEXT, desc TEXT, imageName TEXT, price INTEGER)",
//       'CREATE TABLE $_tableProduct(id INTEGER PRIMARY KEY autoincrement, name TEXT, desc TEXT, imageName TEXT, price INTEGER)',
//     );
//     return;
//   }
//
//   static Future getFileData() async {
//     return getDatabasesPath().then((s) {
//       return path = s;
//     });
//   }
//
//   Future<int> insertProduct(HSProductModel productModel) async {
//     Database db = await instance.database;
//
//     var products = await db.query(_tableProduct,
//         where: "name = ?", whereArgs: [productModel.name]);
//
//     if (products.isNotEmpty) {
//       return -1;
//     }
//
//     return await db.insert(
//       _tableProduct,
//       productModel.convertHSProductModeToMap(),
//     );
//   }
//
//   Future getProductsCount() async {
//     Database db = await instance.database;
//     var res = await db.query(_tableProduct, orderBy: 'name ASC');
//     return res.length;
//   }
//
//   Future<List<HSProductModel>> getProducts() async {
//     Database db = await instance.database;
//     var res = await db.query(_tableProduct, orderBy: 'name ASC'); //DESC ASC
//     List<HSProductModel> products = [];
//     for (int i = 0; i < res.length; i++) {
//       products.add(HSProductModel.fromMap(res[i]));
//     }
//
//     return products;
//   }
//
//   Future deleteProduct(String name) async {
//     Database db = await instance.database;
//     var products =
//         db.delete(_tableProduct, where: "name = ?", whereArgs: [name]);
//     return products;
//   }
// }
