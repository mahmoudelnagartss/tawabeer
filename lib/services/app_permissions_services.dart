import 'package:flutter/cupertino.dart';
import 'package:permission_handler/permission_handler.dart';

class AppPermissionsServices {
  Future<bool> requestCameraPermission() async {
    const servicePermission = Permission.camera;
    PermissionStatus status = await servicePermission.status;

    if (checkPermissionStatus(status)) {
      return true;
    } else {
      status = await Permission.camera.request();

      if (checkPermissionStatus(status)) {
        return true;
      } else {
        await openAppSettings();
        return false;
      }
    }
  }

  Future<bool> requestPhotosPermission() async {
    const servicePermission = Permission.photos;
    PermissionStatus status = await servicePermission.status;

    if (checkPermissionStatus(status)) {
      return true;
    } else {
      status = await Permission.photos.request();

      if (checkPermissionStatus(status)) {
        return true;
      } else {
        await openAppSettings();
        return false;
      }
    }
  }

  Future<bool> requestLocationPermission() async {
    const servicePermission = Permission.location;
    PermissionStatus status = await servicePermission.status;

    if (checkPermissionStatus(status)) {
      return true;
    } else {
      status = await Permission.location.request();

      if (checkPermissionStatus(status)) {
        return true;
      } else {
        await openAppSettings();
        return false;
      }
    }
  }


  bool checkPermissionStatus(PermissionStatus status) {
    debugPrint('Permission $status');

    if (status == PermissionStatus.granted ||
        status == PermissionStatus.limited) {
      return true;
    } else {
      return false;
    }
  }
}
