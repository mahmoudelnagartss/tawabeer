// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:hs_demo/Model/brew_model.dart';
//
// class DatabaseServices {
//   //collection refrence
//   final String? userId;
//   final CollectionReference brewsCollection =
//       FirebaseFirestore.instance.collection('Users');
//
//   DatabaseServices({this.userId});
//
//   Future updateUserData(String suger, String name, int strength) async {
//     return await brewsCollection.doc(userId).set({
//       'suger': suger,
//       'name': name,
//       'strength': strength,
//     });
//   }
//
//   Stream<List<BrewModel>> get brews {
//     return brewsCollection.snapshots().map(getBrewListFromQuerySnapshot);
//   }
//
//   List<BrewModel> getBrewListFromQuerySnapshot(QuerySnapshot querySnapshot) {
//     return querySnapshot.docs
//         .map((doc) => BrewModel.fromQueryDocumentSnapshot(doc))
//         .toList();
//   }
//
//   Stream<BrewModel> get userData {
//     return brewsCollection
//         .doc(userId)
//         .snapshots()
//         .map(getBrewModelFromDocumentSnapshot);
//   }
//
//   BrewModel getBrewModelFromDocumentSnapshot(DocumentSnapshot snapshot) {
//     return BrewModel.fromDocumentSnapshot(snapshot);
//   }
// }
