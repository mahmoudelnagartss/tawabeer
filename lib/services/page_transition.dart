import 'package:flutter/material.dart';

enum TransitionTypes {
  fade,
  scale,
  slideFromBottom,
  slideFromLeft,
  slideFromRight
}

class PageTransition extends PageRouteBuilder {
  final Widget child;
  final TransitionTypes transitionType;
  Duration? duration;

  PageTransition(
      {required this.child, required this.transitionType, this.duration })
      : super(
          transitionDuration: ((duration == null) ? const Duration(milliseconds: 200) : duration),
          pageBuilder: (context, animation, secondaryAnimation) => child,
        );

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    // TODO: implement buildTransitions

    switch (transitionType) {
      case TransitionTypes.scale:
        return ScaleTransition(scale: animation, child: child);
      case TransitionTypes.fade:
        return FadeTransition(opacity: animation, child: child);
      case TransitionTypes.slideFromBottom:
        return SlideTransition(
          position: Tween(
            begin: const Offset(0.0, 1.0),
            end: Offset.zero,
          ).animate(
            animation,
          ),
          child: child,
        );
      case TransitionTypes.slideFromLeft:
        return SlideTransition(
          child: child,
          position: Tween(
            begin: const Offset(-1, 0.0),
            end: Offset.zero,
          ).animate(
            animation,
          ),
        );
      case TransitionTypes.slideFromRight:
        return SlideTransition(
          child: child,
          position: Tween(
            begin: const Offset(1.0, 0.0),
            end: Offset.zero,
          ).animate(
            animation,
          ),
        );
      default:
        return FadeTransition(opacity: animation, child: child);
    }
  }
}
