import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:provider/provider.dart';
import 'package:tawabeer/app%20files/AppProvider.dart';
import 'package:tawabeer/app%20files/my_app_widget.dart';
import 'package:tawabeer/localization/provider/locale_provider.dart';

Future _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  debugPrint("Handling a background message: ${message.messageId}");
}

// initialize firebase app

Future<String> initializeFirebaseMessaging() async {
  await Firebase.initializeApp();
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  await _messaging.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

// receive message while app is working foreground
  FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    debugPrint('Got a message whilst in the foreground!');
    debugPrint('Message data: ${message.data}');

    if (message.notification != null) {
      debugPrint(
          'Message also contained a notification: ${message.notification}');

      // show a notification at top of screen.
      showSimpleNotification(
        ListTile(
          leading: const CircleAvatar(
            radius: 25.0,
            backgroundColor: Colors.brown,
            backgroundImage: AssetImage('lib/assets/coffee_icon.png'),
          ),
          title: Text(message.notification!.title ?? ''),
          subtitle: Text(message.notification!.body ?? ''),
        ),
        duration: const Duration(seconds: 5),
        background: Colors.brown,
      );
    }
  });
// ioadjsdjlakad
  // String token = await _messaging.getToken() ?? '';
  // debugPrint('FirebaseMessaging token : $token');

  return "token";
}

void main() async {
  GestureBinding.instance?.resamplingEnabled = true;
  WidgetsFlutterBinding.ensureInitialized();

  await initializeFirebaseMessaging();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<LocaleProvider>(
          create: (context) => LocaleProvider(),
        ),
        ChangeNotifierProvider<AppProvider>(
          create: (context) => AppProvider(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

// flutter build apk --release
// flutter build apk --release --no-sound-null-safety

// flutter run --release
// flutter run --release --no-sound-null-safety

// open /Applications/Android\ Studio.app

// To get generate a SHA-1 key

// 1- first way
// keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android

// 2-second way
// in the terminal change the path by typeing cd android and then type ./gradlew signingReport

// F8:1A:F7:D8:92:8A:CE:65:F1:34:0D:48:16:20:87:5D:82:AF:F2:88

// for Key Hashes
// keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
