import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class L10n {
  static final all = [
    const Locale('en'),
    const Locale('ar'),
  ];

  static Locale getMainLocal() {
    return const Locale('en');
  }

  static String getName(Locale locale) {
    debugPrint('$locale');
    switch (locale.languageCode) {
      case 'en':
        return 'English';
      case 'en_US':
        return 'English';
      default:
        return 'العربية';
    }
  }

  static String getFlag(Locale locale) {
    debugPrint('$locale');
    switch (locale.languageCode) {
      case 'en':
        return '🇺🇸󠁿';
      case 'en_US':
        return '🇺🇸󠁿';
      default:
        return '🇸🇦';
    }
  }

  static L10n? of(BuildContext context) {
    return Localizations.of<L10n>(context, L10n);
  }

  String taskItems(int taskNumbers) {
    return Intl.plural(taskNumbers,
        zero:
            "You have never written a list of tasks.\nLet's get started soon.",
        one: "This is your todo-list,\nToday, you have 1 task to complete. ",
        many:
            "This is your todo-list,\nToday, you have $taskNumbers tasks to complete. ",
        other:
            "This is your todo-list,\nToday, you have $taskNumbers tasks to complete. ",
        args: [taskNumbers],
        name: "taskItems");
  }

  String get languageTitle {
    return Intl.message(
      'Change Language',
      name: 'languageTitle',
      desc: 'languageTitle',
    );
  }
}
