import 'package:flutter/material.dart';
import 'package:tawabeer/localization/l10n/l10n.dart';
import 'package:tawabeer/services/shared_preferences_services.dart';
import 'dart:async';

class LocaleProvider extends ChangeNotifier {
  Locale _locale = L10n.getMainLocal();

  final SharedPreferencesServices _sharedPreferencesServices =
      SharedPreferencesServices();

  Future<void> setLocale(Locale locale) async {
    if (!L10n.all.contains(locale)) {
      return;
    }

    await _sharedPreferencesServices.setValueForKey(
        'SavedAPPLanguage', locale.languageCode);
    _locale = locale;
    notifyListeners();
  }

  void clearLocale() async {
    await _sharedPreferencesServices.removeValueForKey('SavedAPPLanguage');
    _locale = L10n.getMainLocal();
    notifyListeners();
  }

  Future<Locale> getLocaleFromSharedPreferences() async {
    String languageCode =
        await _sharedPreferencesServices.getSavedValue('SavedAPPLanguage');

    if (languageCode.isNotEmpty) {
      Locale savedLocal = Locale(languageCode);
      _locale = savedLocal;
    }

    return _locale;
  }

  Future<bool> checkLocalSavedLocale() async {
    String languageCode =
    await _sharedPreferencesServices.getSavedValue('SavedAPPLanguage');

    return languageCode.isNotEmpty;
  }
}
